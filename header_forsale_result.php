<?php include 'settings.php' ?>


<!-- Header One Sales
Used on: Index, Selling, Services, Register to Sell, Register to Buy
-->

<link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/styles-results.css">
    <link rel="stylesheet" href="css/queries.css">
    <link rel="stylesheet" href="css/etline-font.css">
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/magnific.css">
    <!-- <script src="./js/magnific.js"></script> -->

    <script src="sweetalert-master/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
    
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


    <link rel="stylesheet" href="css/search.css">
    <script src="js/search.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	 <!-- jQuery -->
    <script src="./bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	
	<script src="./bower_components/bootstrap/js/tab.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

	</head>
<body id="top">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <section class="hero">
        <section class="navigation">
            <header>
                <div class="header-content">
                    <div class="logo"><a href="index.php"><img src="<?php echo $Image; ?>" width="100 px" height="69 px" alt="Trinity Lettings logo"></a></div>
                    <div class="header-nav">
                        <nav>
                            <ul class="primary-nav dropdown">
                            <li><a href="<?php echo $Url; ?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                                <li><a href="index.php#down_scroll">Buying</a></li>
                                <li><a href="selling.php#down_scroll">Selling</a></li>
                                <li><a href="http://trinity.myestatemate.co.uk:8080/">Lettings</a></li>
                                <li><a href="probate.php#down_scroll">Probate</a></li>
                                <li><a href="divorce.php#down_scroll">Divorce</a></li>
                            </ul>
                            <ul class="primary-nav dropdown">
                                <li><a href="mortgages.php#down_scroll">mortgages</a></li>
                                <li><a href="https://www.facebook.com/pg/sell.my.house.in.wakefield/notes/">Blog</a></li>
								<li><a href="contact.php#down_scroll">Contact</a></li>
                                <li id="fat-menu" class="dropdown">
                                  <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">Register<b class="caret"></b></a>
                                  <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regvendor.php#down_scroll">Register to Sell</a></li>
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regbuyer.php#down_scroll">Register to Buy</a></li>
                                  </ul>
                                </li>
                                <li><a href="<?php echo $Facebook; ?>" target="_blank" rel="nofollow"><i class="fa fa-facebook" aria-hidden="true"></i></ a></li>
                                <li><a href="$Twitter" target="_blank" rel="nofollow"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="mailto:<?php echo $Email; ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>

                            </ul>
                            <ul class="member-actions">
                                <li><a href="tel:<?php echo $TrinityPhoneNumber; ?>" class="btn-white btn-small"><?php echo $TrinityPhoneNumberPretty; ?></a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="navicon">
                        <a class="nav-toggle" href="#"><span></span></a>
                    </div>
                 </div>
            </header>
        </section>
    <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="hero-content text-center">
                            <h1>Finding your new home</h1>
                            <p class="intro">

                            <?php 
                            $type = $_GET['type'];
                            if($type == 'ToLet'){
                                include 'searchLet.php';

                            } else {
                                include 'searchLet.php';
                            }
                         ?></p>
                        </div>
                    </div>
                </div>
    </div>
            
            <div class="down-arrow floating-arrow"><a href="#results_scroll"><i class="fa fa-angle-down"></i></a></div>
        </section>
