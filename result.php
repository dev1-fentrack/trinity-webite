<?php

error_reporting(E_ALL);

include "settings.php"

?>



<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Search Properties - Trinity Sales</title>
    <meta name="description" content="">
	<meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php 
    include 'header_forsale.php';
 ?>
    <div class="container main-section">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="hero-content text-center">
                    <h1>Results</h1>
                </div>
            </div>
        </div>
    </div>
 </section>
 <section class="resultarea">
    <div class="container">                       
    <section class="result text-center">
        <div class="container-fluid" id="results_scroll">
            
<?php  
    $SearchCriteria = "";
    if(isset($_GET['SaleLocation'])){
        $SearchCriteria.='&Location='.$_GET['SaleLocation'];
    }

    if(isset($_GET['SaleMinPrice'])){
        $SearchCriteria.='&SaleMinPrice='.$_GET['SaleMinPrice'];
    }

    if(isset($_GET['SaleMaxPrice'])){
        $SearchCriteria.='&SaleMaxPrice='.$_GET['SaleMaxPrice'];
    }

    if(isset($_GET['SaleNumBeds'])){
        $SearchCriteria.='&SaleNumBeds='.$_GET['SaleNumBeds'];
    }

    if(isset($_GET['SalePropType'] )){
        $SearchCriteria.='&SalePropType='.$_GET['SalePropType'];
    }

    if(isset($_GET['LetLocation'])){
        $SearchCriteria.='&Location='.$_GET['LetLocation'];
    }

    if(isset($_GET['LetMinPrice'])){
        $SearchCriteria.='&LetMinPrice='.$_GET['LetMinPrice'];
    }

    if(isset($_GET['LetMaxPrice'])){
        $SearchCriteria.='&LetMaxPrice='.$_GET['LetMaxPrice'];
    }

    if(isset($_GET['LetNumBeds'])){
        $SearchCriteria.='&NumBeds='.$_GET['LetNumBeds'];
    }

    if(isset($_GET['LetPropType'] )){
        $SearchCriteria.='&PropType='.$_GET['LetPropType'];
    }

    //error_log($SearchCriteria);
    //echo 'getting data!!!!';
    //error_log("getting data:"."http://127.0.0.1:8081/GetAssetData.LM.v1.php?Table=".$RecordType.$SearchCriteria);
    $json=file_get_contents("http://127.0.0.1:8081/GetAssetData.LM.v1.php?Table=".$RecordType.$SearchCriteria);
    
    
    error_log('get json');
    //echo 'json:'.$json;
    $data =  json_decode($json);
    //error_log("data:".$data);

    foreach($data as $object){

        $Pic=$object->{'Picture'};
        if($Pic==''){
            $Pic='img/ap.jpg';
        }else{
            $Pic=$Url."/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$Pic;
        }

        $Addr1=$object->{'Addr1'};
        $Addr2=$object->{'Addr2'};
        $PostCode=$object->{'PostCode'};


        if($Addr2 != "" && $Addr1 !=""){
            $Addr1.=", ";
        }
        
        if($PostCode != ""){
            $Addr2.=", ";
        }

        $Phase=$object->{'Phase'};
        //error_log("Phase: ".$Phase);

        $STCString = '';

        if($RecordType == "ForSale"){
            if($Phase == "Sold STC"){
                $STCString = "<h3  style='color:red;'>Sold STC</h3>";
            }
            if($Phase == "Completed"){
                $STCString = "<h3  style='color:red;'>Sold</h3>";
            }
        } else {
            if($Phase == "Let Agreed"){
                $STCString = "<h3  style='color:red;'>Let Agreed</h3>";
            }
            if($Phase == "Let"){
                $STCString = "<h3  style='color:red;'>Let</h3>";
            }
        }
        


        echo '
        <section class="result-intro">
            <div class="container-fluid">
                <div class="row">
                <br>
                    <div class="col-md-3">
                        <article class="result-post">
                            <figure>
                                <a href="'.$Pic.'" class="single_image">
                                    <div class="result-img-wrap">
                                        <a href="'.$Url.'/propertyview.php?Uuid='.$object->{'Uuid'}.'"><img src="'.$Pic.'" height="100%" width="100%" alt=""/></a>
                                        '.$STCString.'
                                    </div>
                                </a>
                            </figure>
                        </article>
                    </div>
                                
                    <div class="col-lg-6"><h3>'.$Addr1.$Addr2.$PostCode.'</h3><p>'.$object->{'FeatureList'}.'<br>';
                if($RecordType==='ForSale'){
                    echo '<h3>£'.$object->{'Price'}.'</h3></p>';
                }else{
                    echo '<h3>£'.$object->{'RentPcm'}.' PCM</h3></p>';
                }



                if($RecordType=== "ToLet"){
                            echo'
                                </div>
                                <div class="col-md-3">
                                    <ul class="">
                                        <li class="active"><a class="btn btn-fill" target="_blank" href="'.$Url.'/propertyview.php?Uuid='.$object->{'Uuid'}.'" >View</a>                   
                                        </li>
                                        <li><a href="tel:'.$TrinityPhoneNumber.'">Call Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </section>
                    ';
                } else {
                    echo'
                                </div>
                                <div class="col-md-3">
                                    <ul class="">
                                        <li class="active"><a class="btn btn-fill" target="_blank" href="'.$Url.'/propertyview.php?Uuid='.$object->{'Uuid'}.'" >View</a>                   
                                        </li>
                                        <li><a href="tel:'.$TrinityPhoneNumber.'">Call Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </section>
                    ';
                }
    }
?>                
                    <!-- Search Result End -->
</div>
           

            <div class="row">
                    <div style="display:none;"  id="noresults"><!-- A no results returned text -->
                 <h3>Sorry!</h3>
                 <p>There is no available properties which match your criteria.<br>
                 If you'd like to receive a weekly email digest of the latest properties we have, which match your current criteria, simply register now</p>
                                        <form class="form-inline">
                                            <div class="form-group">
                                                <label for="exampleInputEmail2"></label>
                                                <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Your email address">
                                            </div>
                                            <button type="submit" class="btn btn-primary">Receive Property Matches</button>
                            </form>
                        <br>
                    </div>
            </div>

	    </div>
        </section>

<?php

include 'footer_forsale.php';

?>