<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Buy with Trinity Sales</title>
    <link rel="stylesheet" href="required.css">

    <meta name="description" content="Trinity Sales, Local Estate Agents in Wakefield, Pontefract, Ossett and Castleford. Find out how we can sell your property in 28 days.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly & for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Register Your Property</h1>
                            <p class="intro">Let Trinity sell your property, promptly, properly and professionally.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-section">
        <div class="container">
            <div class="row text-center">
                <h3>Search for a property</h3>
            </div>
            <div class="row text-center">
                <?php include 'searchLet.php' ?>
            </div>
        </div>
    </section>
                            <section class="features section-padding">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4 intro-feature">

                                            <div class="intro-icon">
                                                <span data-icon="&#xe033;" class="icon"></span>
                                            </div>
                                            <div class="intro-content">
                                                <h5>No Commitments</h5>
                                                <p>Trinity operate very differently to other agencies. Fundamentally, we don't tie you into 6 month contracts, we have contracts for 6 weeks.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 intro-feature">
                                            <div class="intro-icon">
                                                <span data-icon="&#xe030;" class="icon"></span>
                                            </div>
                                            <div class="intro-content">
                                                <h5>Dedicated Tailored Service</h5>
                                                <p>With a personal agent, your property will be put to market and have one call-to point. Meaning our agent knows and deals with every potential bidder for your property. They learn what works best for you and ensures you are happy.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 intro-feature">
                                            <div class="intro-icon">
                                                <span data-icon="&#xe046;" class="icon"></span>
                                            </div>
                                            <div class="intro-content last">
                                                <h5>Peace of Mind</h5>
                                                <p>Trinity Sales handles all the business with very little effort required by yourself. Giving you chance to enjoy the last moments in your current home.</p>
                                            </div>
                                        </div>
                                    </div><div id="down_scroll"></div>
                                </div>
                            </section>
    <section class="intro section-padding" id="contentTop faq_scroll">
         <div class="container">
         <h3>Buyer Registration Form</h3>
            <div class="row">
            

                <div id="form_register">
                    <form class="form-modern" id="commit" name="commit">
                        <div class="row form-div">
                        <div class="col-sm-5 col-md-6 form_register">
                    
                                        
                            <h4>Your Contact Details</h4>
                            <div class="form-input-group">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <input class="form-control" type="text" required="true" tabindex="3002" name="email" id="email" value="" size="35" maxlength="250" placeholder="E-mail" title="Fill in your email address for a response. This is a required field in the form yourname@yourwesite.com">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-address-book" aria-hidden="true"></i>
                                <select id="title" name="title" class="form-control default-option" tabindex="3003" title="Select your title e.g. Mr, Mrs">
                                <option value="" selected="selected">Title</option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Ms">Ms</option>
                                <option value="Miss">Miss</option>
                                <option value="Mr &amp; Mrs">Mr &amp; Mrs</option>
                                <option value="Dr">Dr</option>
                                <option value="Prof">Prof</option>
                                <option value="Dame">Dame</option>
                                <option value="Sir">Sir</option>
                                <option value="Lady">Lady</option>
                                <option value="Lord">Lord</option>
                                <option value="Father">Father</option>
                                <option value="Rev">Rev</option>
                                <option value="Rt Hon">Rt Hon</option>
                                <option value="Sister">Sister</option>
                                </select>
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-address-book" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3004" placeholder="First Name" required="true" name="fname" id="fname" value="" size="15" maxlength="40" title="Fill in your first name. This is a required field.">
                            </div>  
                            
                            <div class="form-input-group">
                                <i class="fa fa-address-book" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3005" placeholder="Surname"  required="true" name="sname" id="sname" value="" size="25" maxlength="40" title="Fill in your lastname/surname. This is a required field.">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3006" placeholder="Phone No. (Day)" required="true" name="phonew" id="phonew" pattern="[0][0-9]{10}" value="" size="35" maxlength="25" title="Fill in your phone for contact by phone. This is your most common contact number mainly your daytime telephone number. Providing your international dial code is also handy">
                            </div>

                        
                            <div class="form-input-group">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3008" name="mobile" placeholder="Phone No. (Mobile)" id="mobile" value="" size="35" pattern="[0][0-9]{10}" maxlength="25" title="Fill in your mobile/cellular for on the road. Providing your international dial code is also handy. This number is handy for calling when you are not at your day time number">
                            </div>

                            <h4>Area Requirements</h4>
                                
                            <div class="form-input-group">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3009" placeholder="Area(s) or a specific street" name="otherareas" id="otherareas" value="" size="35" maxlength="250" title="Fill in a specific area or street requirement">
                            </div>

                            <div class="form-input-group">
                                <i class="fa fa-credit-card" aria-hidden="true"></i>
                                <select class="form-control default-option" tabindex="3012" title="Minimum Price" name="pricelow" id="pricelow">
                                    <option selected="selected" value="">Budget from</option>
                                    <option value="0">No Minimum</option>
                                    <option value="300">£ 300 pcm</option><option value="350">£ 350 pcm</option><option value="400">£ 400 pcm</option><option value="450">£ 450 pcm</option><option value="500">£ 500 pcm</option><option value="550">£ 550 pcm</option><option value="600">£ 600 pcm</option><option value="650">£ 650 pcm</option><option value="700">£ 700 pcm</option><option value="750">£ 750 pcm</option><option value="800">£ 800 pcm</option><option value="850">£ 850 pcm</option><option value="900">£ 900 pcm</option><option value="950">£ 950 pcm</option><option value="1000">£ 1000 pcm</option><option value="1100">£ 1100 pcm</option><option value="1200">£ 1200 pcm</option><option value="1300">£ 1300 pcm</option><option value="1400">£ 1400 pcm</option><option value="1500">£ 1500 pcm</option><option value="1600">£ 1600 pcm</option><option value="1700">£ 1700 pcm</option><option value="1800">£ 1800 pcm</option><option value="1900">£ 1900 pcm</option><option value="2000">£ 2000 pcm</option><option value="2500">£ 2500 pcm</option><option value="3000">£ 3000 pcm</option><option value="3500">£ 3500 pcm</option><option value="4000">£ 4000 pcm</option><option value="4500">£ 4500 pcm</option><option value="5000">£ 5000 pcm</option><option value="6000">£ 6000 pcm</option><option value="7000">£ 7000 pcm</option><option value="8000">£ 8000 pcm</option><option value="9000">£ 9000 pcm</option><option value="10000">£ 10000 pcm</option>
                                </select>
                            </div>
                        
                            <div class="form-input-group">
                                <i class="fa fa-credit-card" aria-hidden="true"></i>
                                <select class="form-control default-option" tabindex="3013" title="Maximum Price" name="pricehigh" id="pricehigh">
                                    <option selected="selected" value="">Budget to</option>
                                    <option value="0">No Maximum</option>
                                    <option value="300">£ 300 pcm</option><option value="350">£ 350 pcm</option><option value="400">£ 400 pcm</option><option value="450">£ 450 pcm</option><option value="500">£ 500 pcm</option><option value="550">£ 550 pcm</option><option value="600">£ 600 pcm</option><option value="650">£ 650 pcm</option><option value="700">£ 700 pcm</option><option value="750">£ 750 pcm</option><option value="800">£ 800 pcm</option><option value="850">£ 850 pcm</option><option value="900">£ 900 pcm</option><option value="950">£ 950 pcm</option><option value="1000">£ 1000 pcm</option><option value="1100">£ 1100 pcm</option><option value="1200">£ 1200 pcm</option><option value="1300">£ 1300 pcm</option><option value="1400">£ 1400 pcm</option><option value="1500">£ 1500 pcm</option><option value="1600">£ 1600 pcm</option><option value="1700">£ 1700 pcm</option><option value="1800">£ 1800 pcm</option><option value="1900">£ 1900 pcm</option><option value="2000">£ 2000 pcm</option><option value="2500">£ 2500 pcm</option><option value="3000">£ 3000 pcm</option><option value="3500">£ 3500 pcm</option><option value="4000">£ 4000 pcm</option><option value="4500">£ 4500 pcm</option><option value="5000">£ 5000 pcm</option><option value="6000">£ 6000 pcm</option><option value="7000">£ 7000 pcm</option><option value="8000">£ 8000 pcm</option><option value="9000">£ 9000 pcm</option><option value="10000">£ 10000 pcm</option>
                                </select>
                            </div>
                            
                            <div class="form-input-group">
                                <i class="fa fa-bed" aria-hidden="true"></i>
                                <select class="form-control default-option" tabindex="3014" name="propbedr" id="propbedr" title="Select the number of bedrooms you require">
                                    <option selected="selected" value="">Number of bedrooms</option>
                                    <option value="0">Studio</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5+">5+</option>
                                </select>
                            </div>
                            
                            <div class="form-input-group">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <select class="form-control default-option" tabindex="3015" name="proptype" id="proptype" title="Select the type or style of property required">
                                    <option selected="selected" value="">Type of property required</option>
                                    <option value="Flat">Flat</option>
                                    <option value="House">House</option>
                                    <option value="Studio">Studio</option>
                                    <option value="Duplex">Duplex</option>
                                    <option value="Barn Conversion">Barn Conversion</option>
                                    <option value="House Share">House Share</option>
                                    <option value="Houseboat">Houseboat</option>
                                    <option value="Mobile Home">Mobile Home</option>
                                    <option value="Apartment">Apartment</option>
                                    <option value="Bedsit">Bedsit</option>
                                    <option value="Building Plot">Building Plot</option>
                                    <option value="Conversion">Conversion</option>
                                    <option value="Cottage">Cottage</option>
                                    <option value="Detached">Detached</option>
                                    <option value="End Of Terrace">End Of Terrace</option>
                                    <option value="Flatshare">Flatshare</option>
                                    <option value="Land">Land</option>
                                    <option value="Loft">Loft</option>
                                    <option value="Maisonette">Maisonette</option>
                                    <option value="Mansion Block">Mansion Block</option>
                                    <option value="Mews">Mews</option>
                                    <option value="Penthouse">Penthouse</option>
                                    <option value="Purpose Built">Purpose Built</option>
                                    <option value="Retirement">Retirement</option>
                                    <option value="Room To Let">Room To Let</option>
                                    <option value="Semi Detached">Semi Detached</option>
                                    <option value="Serviced Apartment">Serviced Apartment</option>
                                    <option value="Terraced">Terraced</option>
                                    <option value="Town House">Town House</option>
                                    <option value="Garage">Garage</option>
                                    <option value="Parking">Parking</option>
                                    <option value="Bungalow">Bungalow</option>
                                    <option value="Commercial">Commercial</option>
                                    <option value="Light Industrial">Light Industrial</option>
                                    <option value="Live/Work">Live/Work</option>
                                    <option value="Office">Office</option>
                                    <option value="Public House">Public House</option>
                                    <option value="Restaurant">Restaurant</option>
                                    <option value="Shop">Shop</option>
                                    <option value="Studio Space">Studio Space</option>
                                    <option value="Warehouse">Warehouse</option>
                                    <option value="Warehouse Conversion">Warehouse Conversion</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-5 col-md-6 form_register">
                            <div class="form-group row">
                                <div class="col-md-8 checkdiv">
                                    <label class="form-check-label" for="valuation" title="Would you like us to value your property for sale?">Would you like a valuation of your property?</label>
                                </div>
                                <div class="col-md-4">
                                    <input class="option-input checkbox" type="checkbox" tabindex="3033" name="valuation" id="valuation" value="valuation" title="Would you like us to value your property for sale?">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-8 checkdiv">
                                    <label class="form-check-label" for="mailinglist" title="Do you wish to receive properties &amp; updates by email?">Tick to receive properties &amp; updates by email?</label>
                                </div>
                                <div class="col-md-4">
                                    <input class="option-input checkbox" type="checkbox" tabindex="3034" name="mailinglist" id="mailinglist" value="mailinglist" title="Do you wish to receive properties &amp; updates by email?">
                                </div>
                            </div>

                            <h4>Your Current Address</h4>

                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3035" placeholder="Property Number or Name" name="address0" id="address0" value="" size="25" maxlength="40" title="Fill in your building or house name/number">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3036" placeholder="Street Name" name="address3" id="address3" value="" size="25" maxlength="60" title="Fill in your street name">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3037" placeholder="Town/City" name="address5" id="address5" value="" size="35" maxlength="60" title="Fill in your Town or City">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3038" placeholder="County" name="address6" id="address6" value="" size="35" maxlength="60" title="Fill in your county">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3039" placeholder="Postcode" name="postcode" id="postcode" value="" size="15" maxlength="12" title="Fill in your post code">
                            </div>

                            <h4>Additional Message, Notes or Comments</h4>
                            <div class="form-input-group">
                                <i class="fa fa-sticky-note" aria-hidden="true"></i>
                                <textarea class="form-control"  tabindex="3044" placeholder="Notes" name="notes" id="notes" class="form_register_textarea" title="Type in any additional Message, Notes or Comments" rows="8" cols="70"></textarea>
                            </div>
                                        
                        <script>
                            function alertmsg() {

                                swal("Thank you", "Will will be in touch shortly");
                            }
                        </script>
                        <fieldset class="form-group">
                            <input type="hidden" value="ToLet" name="site" id="site">
                        </fieldset>
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 submit-div">
                        <button type="submit" name="submit"  value="Submit" title="Click to send your registration" id="form_register_submitbutton" class="btn-fill btn-submit" onclick="if(PostRecord('BuyerRegistration', '#commit')){alertmsg();} return false;">Submit</button>
                    </div>
                </div>
            </form>

                        			
            </div>
		</div>		
			
    </div>
</section>
<?php include 'footer_forsale.php' ?>