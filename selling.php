<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Estate Agents for West Yorkshire - Trinity Sales</title>
    <meta name="description" content="Sell in days not months for the market price. Call Trinity Sales Estate Agents Wakefield on 01924 609811 to sell in 28 days.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Trinity Estate Agents</h1>
                        <p class="intro">West Yorkshire Estate Agents with a difference</p>
                        <a href="request_appraisal.php" class="btn btn-lg btn-fill" >BOOK YOUR FREE APPRAISAL NOW</a> <br/>
    </section>
<section class="search-section">
    <div class="container">
        <div class="row text-center">
            <h3>Search for a property</h3>
        </div>
        <div class="row text-center">
            <?php include 'searchLet.php' ?>
        </div>
    </div>
</section>
<?php include 'testimonials.php' ?>
    <section class="selling-video">
        <div class="container">
            <iframe style="display: block; margin: 30px auto;" src="https://player.vimeo.com/video/92036600?color=bdfa05&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </section>
    <section class="hero-strip section-padding">
        <div class="container">
            <div class="col-md-12 text-center">
                <h3 style="color:white;">
                Get Your Free Guide – The Elite Home Sellers Bible
                </h3>
                <p>The Six Secrets To Sell Your Property Full Value & Fast</p>
                <button class="btn btn-fill btn-large" onclick="window.open('http://sk123-226928.lpp.infusionsoft.com', '_blank')" type="button">
     Free Guide!</button>
                <div class="logo-placeholder floating-logo"><img class="img-float" src="img/house.png" alt=""></div>
            </div>
        </div>
    </section>
    <section class="features section-padding" id="sell_scroll">
        <div class="container">
            
            <h3>Benefits of selling with Trinity</h3>
             
            <div class="row">
                <div class="col-md-4 intro-feature">
                    <div class="intro-content">
                        <h5>FASTER SALES</h5>
                        <p>Most of our sales are agreed in the first 4 weeks from listing. This is because our experienced sale valuers give you honest valuations and back it up with superior marketing strategies.  Our team proactively calls out to interested buyers daily to drive viewings and increase the speed of agreeing your sale.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-content">
                        <h5>FULL VALUE SALES</h5>
                        <p>We pride ourselves on our six week contract versus industry standard of six months. We'll get your property sold quicker and for the best price.Unlike the “old school” agents who over value to win business and then push for price reductions which can mean you end up giving your property away on the cheap.  Our experienced local valuers will give you honest valuations and tell you the best marketing strategies to use to get a fast but full value sale.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-content last">
                        <h5>GREATER CERTAINTY OF SALE</h5>
                        <p>Roughly 1/3 of sales collapse industry wide. But not at Trinity.  Its closer to 1 in 10.  Why?  Because every sale matters to us. Thorough offer screening combined with pro-active sales progression means if your sale collapses it wont be through incompetence.</p>
                    </div>
                </div>
            </div>
					
        </div>
    </section>

    <section class="blog text-center">
        <div class="container-fluid">
            <div class="row">
                <?php

    $json=file_get_contents("/GetFeaturedData.LM.v1.php?Table=ForSale");

    $data =  json_decode($json);

        $x = 0;
        foreach($data as $object){

        if($x > 0){
            $Pic=$object->{'Picture'};
            if($Pic==''){
                $Pic='img/ap.jpg';
            }else{
                $Pic="http://manyclicks.co.uk/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$Pic;
            }

            $Uuid=$object->{'Uuid'};
            $Addr2=$object->{'Addr2'};
            $Addr3=$object->{'Addr3'};

            if($Addr3 != '' | $Addr3 != ' '){
                $Addr2.=", ";
            }
            $BuildType=$object->{'BuildType'};
            
     

            echo '  <div class="col-md-4">
                    <article class="blog-post">
                        <figure>
                            <a href="'.$Pic.'" class="single_image">
                                <div class="blog-img-wrap">
                                    <div class="overlay">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    <img src="'.$Pic.'" alt=""/>
                                </div>
                            </a>
                            <figcaption>
                            <h3><a  class="blog-category" >'.$Addr2.$Addr3.'</a></h3>
                            <p><a href="'.$ForSaleUrl.'/propertyview.php?Uuid='.$Uuid.'" class="blog-post-title">'.$BuildType.'<i class="fa fa-angle-right"></i></a></p>
                            </figcaption>
                        </figure>
                    </article>
                </div>';

                
        }
        $x++;
    }


?>

            </div>
        </div>
    </section>
<?php include 'footer_forsale.php' ?>