<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Privacy Statement - Trinity Sales</title>
    <meta name="description" content="">
	<meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Privacy Statement</h1>
                        <p class="intro">We respect your privacy. Any information you provide is confidential</p>
                        <a href="#faq_scroll" class="btn btn-fill btn-large btn-margin-right">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="down-arrow floating-arrow"><a href="#"><i class="fa fa-angle-down"></i></a></div>
    </section>
        <section class="features section-padding" id="contentTop faq_scroll">
        <div class="container">
            <div class="row">
				<div>
				<p>Trinity Lettings has created this privacy statement in order to demonstrate our firm commitment to privacy. The following discloses our information gathering and dissemination practices for this web site. Any and all information collected at this site will be kept strictly confidential and will not be sold, reused, rented, loaned, or otherwise disclosed to any undisclosed third party. Any information you give to Trinity Lettings will be held with the utmost care, and will not be used in ways that you have not consented to.
A more detailed explanation of our policy and about how we safeguard your personal information is shown below.
Submitting Registration Information
The site registration forms requires users to supply contact information like their name, telephone number, and email address. By submitting the registration information via this web site the user agrees that the Estate Agent can use this information to contact the user and match requirements.
<h4>Removal from list</h4>
This site gives users the following option to remove their information from the registration list and to not receive future communications from the agent.
You can send an email to <?php echo $ToLetEmail; ?> 
The subject line should contain only one word: REMOVE
<h4>Resale or disclosure of information</h4>
We do not sell, rent, loan, trade, or lease any personal information collected at our site. Information is passed through the www.trinitylettings.com site and given to the agent. The email address of the Estate Agent(s) who receive the information that the user supplies are shown at the top of the registration form confirmation, once submitted.
What information is automatically collected when you visit this site
When you browse on this site your e-mail address is not automatically collected but our server may automatically gather and store the following information about your visit:
<ol>
<li>Your Internet domain name and IP address.</li>
<li>The type of browser and operating system used to access our site.</li>
<li>The pages you visit.</li>
<li>The date and time of your visit.</li>
</ol>
We analyse our web site logs to constantly improve the value of the materials available on web site. Our web site logs are not personally identifiable, and we make no attempt to link them with the individuals that actually browse the site. We use your IP address to help diagnose problems with our server, and to administer our Web site. An IP address only provides information about who your Internet service provider is. It cannot be used to personally identify you.
<h4>Twitter</h4>
Any details (including email address) collected using Twitter lead generation cards will only be used by Trinity Residential Solutions ltd for legitimate business purposes and details won’t be passed onto third parties without permission.
<h4>Use of Cookies</h4>
A cookie is a small string of text that a Web site can send to your browser to assist in session-state management or store user information for the purposes of distinguishing your browser from a previous visitor by saving and remembering any preferences or passwords that may have been set while browsing that particular site.
Currently this site uses cookies for session-state management and user tracking analysis (Google Analytics). We have no current plan to use cookies for any other purpose. When cookie technology is used, the main purpose is to store user identification information to allow you to have a better experience when using this site. We will revise this policy statement accordingly if we elect to begin using cookie technology for any other purpose, so you may wish to check this policy periodically for updates
<h4>Security</h4>
Your information submitted to us will be stored on our servers. Our Internet hardware is maintained in a secure environment, designed to be accessed only by authorized personnel.
<h4>Contacting the Web Site</h4>
If you have any questions about this privacy statement, the practices of this site, or your dealings with this Web site, you can contact:

Trinity Lettings 
586 Leeds Road, Outwood, Wakefield, West Yorkshire, WF1 2LT 
Email: <strong><a href="mailto:<?php echo $ToLetEmail; ?>"><?php echo $ToLetEmail; ?></a></strong> 
Telephone: <strong><a href="tel:<?php $TrinityPhoneNumber; ?>"><?php echo $TrinityPhoneNumberPretty; ?></a></p></strong>

				</div>
			</div>				
			
                </div>
            </div>
        </div>
    </section>
<?php include 'footer_forsale.php' ?>