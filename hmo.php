<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>HMO - Trinity Sales</title>
    <link rel="stylesheet" href="required.css">
    
    <meta name="description" content="Contact Trinity Sales for your Buying or Selling needs. Independent Wakefield Estate Agency.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly & for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>HMO</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-section">
        <div class="container">
            <div class="row text-center">
                <h3>Search for a property</h3>
            </div>
            <div class="row text-center">
                <?php include 'searchLet.php' ?>
            </div>
        </div>
    </section>
     <section id="first_scroll" class="testimonial-slider section-padding text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flexslider">
                       <ul class="slides"> 
                             <li>
                                <h3>"I inherited a 10 Bed HMO which needed lots of work to get it habitable and pass for a HMO licence. Being based in london management was near impossible. The existing tenants were not paying their rent and the whole property was deteriorating. Trinity helped me get rid of the bad tenants, arrange all the improvements necessary to gain the licence and attract a better standard of tenant who pays their rent on time. I'd like to thank Chelsee and the team at trinity for all their help and great advice."</h3>
                                <p class="author">M. Berger</p>
                                <div class="rating"><img src="img/5stars.png" alt=""></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="intro section-padding" id="down_scroll">
        <section class="features section-padding" >
        <div class="container">
            <div class="row request-div">
                <a href="contact.php#form_scroll" class="btn btn-fill btn-large">Request Information</a>
            </div>
            <div class="row" style="margin-top:50px;">
                <div class="col-md-8 intro-feature">
                    <div class="intro-content ">
                        <p>HMO’s require specialist management and marketing in order for them to run smoothly and minimise voids.</p>
                    </div>
                    <div class="intro-content last ">
                        <h5>Dedicated Tailored Service</h5>
                        <p>Many estate agents say they do HMO’s but in reality most try to manage & market them the same as they would a single let property and they market them on the web portals where typically room hunters don’t look</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <img class="intro-img" src="../img/hmo.jpg">
                </div>
            </div>
        </div>
    </section>
   
    
<?php include 'footer_forsale.php' ?>