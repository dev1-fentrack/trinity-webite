<!-- Header 2 is used for contact page only -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/styles-contact.css">
    <link rel="stylesheet" href="css/queries.css">
    <link rel="stylesheet" href="css/etline-font.css">
    <link rel="stylesheet" href="css/forms.css">
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    
    <link rel="stylesheet" href="css/search.css">
    <script src="js/search.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	 <!-- jQuery -->
    <script src="./bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	
	<script src="./bower_components/bootstrap/js/tab.js"></script>
	
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
	</head>
<body id="top">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <section class="hero">
        <section class="navigation">
            <header>
                <div class="header-content">
                    <div class="logo"><a href="index.php"><img src="img/trinity-sales1.png" width="100 px" height="69 px" alt="Trinity Lettings logo"></a></div>
                    <div class="header-nav">
                        <nav>
                            <ul class="primary-nav dropdown">
                                <li><a href="index.php#buy_scroll">Buying</a></li>
                                <li><a href="selling.php">Selling</a></li>
                                <li><a href="services.php">Services</a></li>
                                <li><a href="contact.php">Contact</a></li>
                                <li id="fat-menu" class="dropdown">
                                  <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">Register<b class="caret"></b></a>
                                  <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regvendor.php">Register to Sell</a></li>
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regbuyer.php">Register to Buy</a></li>
                                  </ul>
                                </li>
                                <li><a href="http://manyclicks.co.uk:8080">Properties to Let</a></li>
                                </ul>
                            <ul class="member-actions">
                                <li><a href="tel:+441924609811" class="btn-white btn-small">01924 609 811</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="navicon">
                        <a class="nav-toggle" href="#"><span></span></a>
                    </div>
                </div>
            </header>
        </section>
