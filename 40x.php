<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Oops! Something went wrong.</title>
    <meta name="description" content="">
	<meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>404 - Something is missing</h1>
                        <p class="intro">It's probably our fault, sorry.</p>
                        <a href="index.php" class="btn btn-fill btn-large btn-margin-right">Return Home</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="down-arrow floating-arrow"><a href="#"><i class="fa fa-angle-down"></i></a></div>
    </section>
   
    <section class="features section-padding" id="contentTop faq_scroll">
        <div class="container">
            <div class="row">
				<div>
					<h2>Something has gone wrong</h2>
					<p>It is very likely our fault as to why you are viewing this message. We've recorded your presence here and we will seek to fix the issue very shortly.
					If you would like to return <a href="index.php">home</a> or wish to get in <a href="/contact">contact</a> that would be great. Thank you.</p>

				</div>
			</div>				
			
                </div>
            </div>
        </div>
    </section>
<?php include 'footer_forsale.php' ?>