<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="required.css">

    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Become a Landlord with Trinity Lettings</title>
    <meta name="description" content="Become a tenant of your next dream home with Trinity Lettings. Register and we will inform you of all your criteria matches!">
	<meta name="keywords" content="Flat to rent in Leeds, House to rent in Wakefield, Property to rent Leeds Wakefield, letting in Leeds Wakefield, letting in Leeds, lettings in Wakefield, lettings in Leeds, Lettings Agents">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Trinity Finds Perfect Tenants</h1>
                        <p class="intro">Trust us for peace of mind and easy returns on your investment.</p>
	                </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-section">
        <div class="container">
            <div class="row text-center">
                <h3>Search for a property</h3>
            </div>
            <div class="row text-center">
                <?php include 'searchLet.php' ?>
            </div>
        </div>
    </section>
    <section class="intro section-padding">
        <div class="container">
        <h3>Landlord Registration</h3>
            <div class="row">
                    						
					
					<form class="form-modern" id="commit" name="commit">
						<div class="row form-div">
						<div class="col-sm-5 col-md-6 form_register">
							<script src="/js/register_validate.js" type="text/javascript"></script>
							<h4>Your Contact Details</h4>
							
		  					
							<input class="form-control" type="hidden" value="ToLet" name="site" id="site">
							<div class="form-input-group">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <input class="form-control" type="text" required="true" tabindex="3002" name="email" id="email" value="" size="35" maxlength="250" placeholder="E-mail" title="Fill in your email address for a response. This is a required field in the form yourname@yourwesite.com">
                            </div>
							
							<div class="form-input-group">
			                    <i class="fa fa-address-book" aria-hidden="true"></i>
			                    <select id="title" name="title" class="form-control default-option" tabindex="3003" title="Select your title e.g. Mr, Mrs">
			                    <option value="" selected="selected">Title</option>
			                    <option value="Mr">Mr</option>
			                    <option value="Mrs">Mrs</option>
			                    <option value="Ms">Ms</option>
			                    <option value="Miss">Miss</option>
			                    <option value="Mr &amp; Mrs">Mr &amp; Mrs</option>
			                    <option value="Dr">Dr</option>
			                    <option value="Prof">Prof</option>
			                    <option value="Dame">Dame</option>
			                    <option value="Sir">Sir</option>
			                    <option value="Lady">Lady</option>
			                    <option value="Lord">Lord</option>
			                    <option value="Father">Father</option>
			                    <option value="Rev">Rev</option>
			                    <option value="Rt Hon">Rt Hon</option>
			                    <option value="Sister">Sister</option>
			                    </select>
			                </div>

							<div class="form-input-group">
			                    <i class="fa fa-address-book" aria-hidden="true"></i>
			                    <input class="form-control" type="text" tabindex="3004" placeholder="First Name" required="true" name="fname" id="fname" value="" size="15" maxlength="40" title="Fill in your first name. This is a required field.">
			                </div>	
							
							<div class="form-input-group">
			                    <i class="fa fa-address-book" aria-hidden="true"></i>
			                    <input class="form-control" type="text" tabindex="3005" placeholder="Surname"  required="true" name="sname" id="sname" value="" size="25" maxlength="40" title="Fill in your lastname/surname. This is a required field.">
			                </div>
								
							<div class="form-input-group">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3006" placeholder="Phone No. (Day)" required="true" name="phonew" id="phonew" pattern="[0][0-9]{10}" value="" size="35" maxlength="25" title="Fill in your phone for contact by phone. This is your most common contact number mainly your daytime telephone number. Providing your international dial code is also handy">
                            </div>

						
							<div class="form-input-group">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3008" name="mobile" placeholder="Phone No. (Mobile)" id="mobile" value="" size="35" pattern="[0][0-9]{10}" maxlength="25" title="Fill in your mobile/cellular for on the road. Providing your international dial code is also handy. This number is handy for calling when you are not at your day time number">
                            </div>

							<input type="hidden" tabindex="3009" name="pricetype" value="3">
				            <div class="form-input-group">
                                <i class="fa fa-question" aria-hidden="true"></i>
                                <select class="form-control default-option" tabindex="3010" name="source" id="source" title="How did you hear about us?">
                                	<option value="" selected>How did you hear about us&nbsp;?</option>
									<option value="Newspaper">Newspaper</option>
									<option value="Social Networks">Social Networks</option>
									<option value="PrimeLocation">PrimeLocation.com</option>
									<option value="PropertyIndex">PropertyIndex.co.uk</option>
									<option value="Look4Property">Look4Property.com</option>
									<option value="Homeflow">Homeflow.co.uk</option>
									<option value="Movewithus">Movewithus.co.uk</option>
									<option value="NetHousePrices">NetHousePrices.co.uk</option>
									<option value="NAEA">NAEA.co.uk</option>
									<option value="LettingWeb">LettingWeb.co.uk</option>
									<option value="Home">Home.co.uk</option>
									<option value="OnTheMarket">OnTheMarket.com</option>
									<option value="Board">Board</option>
									<option value="Canvass">Canvass</option>
									<option value="Our Website">Our Website</option>
									<option value="Referral">Referral</option>
									<option value="Walk in">Walk in</option>
									<option value="Loot">Loot</option>
									<option value="Internet">Internet</option>
									<option value="FindAProperty">Internet Enquiry - FindAProperty.co.uk</option>
									<option value="Globrix">Internet Enquiry - Globrix.com</option>
									<option value="GuildProperty">Internet Enquiry - GuildProperty.co.uk</option>
									<option value="Gumtree">Internet Enquiry - Gumtree.com</option>
									<option value="Homes24">Internet Enquiry - Homes24.co.uk</option>
									<option value="HomesAndProperty">Internet Enquiry - HomesAndProperty.co.uk</option>
									<option value="HouseLadder">Internet Enquiry - HouseLadder.co.uk</option>
									<option value="LettingSearch">Internet Enquiry - LettingSearch.co.uk</option>
									<option value="Net-Lettings">Internet Enquiry - Net-Lettings.co.uk</option>
									<option value="RadarHomes">Internet Enquiry - RadarHomes.co.uk</option>
									<option value="RentRight">Internet Enquiry - RentRight.co.uk</option>
									<option value="Rightmove">Internet Enquiry - Rightmove.co.uk</option>
									<option value="Zoopla">Internet Enquiry - Zoopla.co.uk</option>
                                </select>
                            </div>

				   		</div>

				   		<div class="col-sm-5 col-md-6 form_register">
				   			<div class="form-group row">
								<div class="col-md-8 checkdiv">
									<label class="form-check-label" for="valuation" title="Would you like us to value your property for sale?">Would you like a valuation of your property?</label>
								</div>
							    <div class="col-md-4">
							    	<input class="option-input checkbox" type="checkbox" tabindex="3033" name="valuation" id="valuation" value="valuation" title="Would you like us to value your property for sale?">
							    </div>
							</div>
						    <h4>Property Address</h4>
								            
	  					
                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3012" placeholder="Property Number or Name" name="address0" id="address0" value="" size="25" maxlength="40" title="Fill in your building or house name/number">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3014" placeholder="Street Name" name="address3" id="address3" value="" size="25" maxlength="60" title="Fill in your street name">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3016" placeholder="Town/City" name="address5" id="address5" value="" size="35" maxlength="60" title="Fill in your Town or City">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3017" placeholder="County" name="address6" id="address6" value="" size="35" maxlength="60" title="Fill in your county">
                            </div>
                            <div class="form-input-group">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <input class="form-control" type="text" tabindex="3018" placeholder="Postcode" name="postcode" id="postcode" value="" size="15" maxlength="12" title="Fill in your postal code">
                            </div>

		            		<h4>Additional Message</h4>
		            		<div class="form-input-group">
			                    <i class="fa fa-sticky-note" aria-hidden="true"></i>
			                    <textarea tabindex="3020" name="notes" id="notes" placeholder="Notes" class="form_register_textarea form-control" title="Type in any additional Message, Notes or Comments" rows="5" cols="70"></textarea>
			                </div>
							<script>
		                        function alertmsg() {

		                            swal("Thank you", "Will will be in touch shortly");
		                        }

		                    </script>

							
					
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 submit-div">
							<button class="btn-fill btn-submit" type="submit" name="submit"  value="Submit" title="Click to send your registration" id="form_register_submitbutton" class="sitebuttonnormal regSubmit"  onclick="if(PostRecord('LandlordRegistration', '#commit')){alertmsg();} return false;">Submit</button>
						</div>
					</div>
			</form>
		</div>
		</div>
    </section>
<?php include 'footer_forsale.php' ?>