    <section class="to-top">
        <div class="container">
            <div class="row">
                <div class="to-top-wrap">
                    <a href="#top" class="top"><i class="fa fa-angle-up"></i></a>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="footer-links">
                        <ul class="footer-group">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="index.php#buy_scroll">Buying</a></li>
                                <li><a href="selling.php">Selling</a></li>
                                <li><a href="services.php">Services</a></li>
                                <li><a href="contact.php">Contact</a></li>
                                <li><a href="regvendor.php">Register to Sell</a></li>
                                <li><a href="regbuyer.php">Register to Buy</a></li>
                                <li><a href="http://manyclicks.co.uk:8080/">Properties to Let</a></li>
                                <li><a href="privacy.php">Privacy Policy</a></li>
                        </ul>
                        <p>Copyright © 2016 <a href="#">Trinity Sales</a><br>
						<div>
                        <img src="./img/rightmove.png" height="100%" width="150px" alt="Rightmove" />
                        <img src="./img/zoopla.png" height="100%" width="150px" alt="Zoopla" />
						<img src="./img/propertyosales.jpg" height="100%" width="150 px" alt="Property Ombudsman" />
                        <img src="./img/naea.jpg" height="100%" width="125px" alt="National Association of Estate Agents" />
						</div>
                    </div>
                </div>
                <div class="col-md-3 social-share">
                    <p>Share Trinity Sales with friends</p>
                    <a href="https://twitter.com/Sales_Trinity" class="twitter-share"><i class="fa fa-twitter"></i></a> <a href="https://www.facebook.com/sell.my.house.in.wakefield/" class="facebook-share"><i class="fa fa-facebook"></i></a>
                </div>
            </div>
        </div>
        </div>
    </footer>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="bower_components/retina.js/dist/retina.js"></script>
    <script src="js/jquery.fancybox.pack.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="bower_components/classie/classie.js"></script>
    <script src="bower_components/jquery-waypoints/lib/jquery.waypoints.min.js"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    

    function AddArg(url,field){

        var t=document.getElementById(field).value;
        if(t!=''){
            url+='&';
            url+=field+"="+t;
        }
        return url;
    }


    function SearchToLet(){
        var url='http://manyclicks.co.uk:8080/result.php?type=ToLet';
        
        url=AddArg(url,"LetLocation");
        url=AddArg(url,"LetMinPrice");
        url=AddArg(url,"LetMaxPrice");
        url=AddArg(url,"LetNumBeds");
        url=AddArg(url,"LetPropType");
        
        location.href=url;
    }
    function SearchForSale(){
        var url='http://manyclicks.co.uk:8081/result.php?type=ForSale';
        
        url=AddArg(url,"SaleLocation");
        url=AddArg(url,"SaleMinPrice");
        url=AddArg(url,"SaleMaxPrice");
        url=AddArg(url,"SaleNumBeds");
        url=AddArg(url,"SalePropType");
        

        location.href=url;
    }

    </script>

</body>
</html>