     <section id="first_scroll" class="testimonial-slider section-padding text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flexslider">
                       <ul class="slides"> 
                             <li>
                                <h2>"Trinity sold my house within 48 hours of signing up. I highly recommend them."</h2>
                                <p class="author">Miss A Richardson, Outwood</p>
                                <div class="rating"><img src="img/5stars.png" alt=""></div>
                            </li>
                            <li>
                                <h2>"Our buyers were talking about pulling out due to a potential issue with Yorkshire Water. Trinity went above and beyond in order to fix the issue and get my sale over the line."</h2>
                                <p class="author">Mrs S Foreman, Stanley</p>
                                <div class="rating"><img src="img/5stars.png" alt=""></div>
                            </li>
                            <li>
                                <h2>"We had been with three different agents and all they seemed to suggest was reducing the price. Trinity suggested a strategy that made a lot of sense and we agreed a sale in 4 weeks."</h2>
                                <p class="author">Mr H Tolan, Ossett</p>
                                <div class="rating"><img src="img/5stars.png" alt=""></div>
                            </li>
                            <li>
                                <h2>"I was really impressed with Dominic and the team at Trinity.  They were friendly, knowledgeable and easy to work with. Once we signed, they were on it immediately and we agreed a sale within the month. They’re now selling my sister's other house too."</h2>
                                <p class="author">Mr C Goodwin, Lofthouse</p>
                                <div class="rating"><img src="img/5stars.png" alt=""></div>
                            </li>
                            <li>
                                <h2>"I was after a buy to let property. Not only did Trinity help me with the purchase but they had tenants ready to move in when we completed so it wasn’t sitting empty."</h2>
                                <p class="author">A Patel, Wrenthorpe</p>
                                <div class="rating"><img src="img/5stars.png" alt=""></div>
                            </li>
                            <li>
                                <h2>"After switching agents to Trinity and using their advice we had 4 viewings in the first 48 hours and managed to sell for £1,000 more than we needed."</h2>
                                <p class="author">Mr Esberger, Hanging Heaton</p>
                                <div class="rating"><img src="img/5stars.png" alt=""></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>