<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Estate Agents for West Yorkshire - Trinity Sales</title>
    <meta name="description" content="Sell in days not months for the market price. Call Trinity Sales Estate Agents Wakefield on 01924 609811 to sell in 28 days.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Trinity Estate Agents</h1>
                        <p class="intro">West Yorkshire Estate Agents with a difference</p>
                        <a href="regbuyer.php#down_scroll" class="btn btn-lg btn-fill" >Register as Buyer</a> <br/>
    </section>
    <section class="search-section">
        <div class="container">
            <div class="row text-center">
                <h3>Search for a property</h3>
            </div>
            <div class="row text-center">
                <?php include 'searchLet.php' ?>
            </div>
        </div>
    </section>
    <?php include 'testimonials.php' ?>
   <section class="intro section-padding" >
        <div class="container">
            <h3>Steps For A Smooth Buying Process</h3>

            <div class="row">
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe007;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>KNOW WHAT YOU CAN BORROW</h5>
                        <p>There’s nothing worse than finding your ideal property and being turned down by your bank.  Its easy to avoid though.  Step 1 should be to speak to an Independent Mortgage Broker rather than your bank.  Your broker will search the whole market for you to find you the best deal. And more importantly tell you what you can borrow. What’s more you’ll only pay them if your mortgage is successful.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe00d;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>REGISTER YOUR BUYING REQUIREMENTS WITH US</h5>
                        <p>Some properties are sold before they get on Rightmove.  If we know what you’re looking for we can let you know about any new instructions which match your needs and book you in first.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe04e;" class="icon"></span>
                    </div>
                    <div class="intro-content last">
                        <h5>GET YOUR AGREEMENT IN PRINCIPLE BEFORE OFFERING</h5>
                        <p>When we’re putting offers forwards we cannot secure the property for you and take it off the market until we have received a valid agreement in principle and proof of funds.  If you’ve used a mortgage broker they’ll send this same day for you putting you in the strongest position to get your offer accepted and the property off the market.</p>
                    </div>
                </div>
            </div>
        </div><div id="appraisal_scroll"></div>
    </section>

    <section class="blog text-center">
        <div class="container-fluid">
            <div class="row">
                <?php

    $json=file_get_contents("/GetFeaturedData.LM.v1.php?Table=ForSale");

    $data =  json_decode($json);

        $x = 0;
        foreach($data as $object){

        if($x > 0){
            $Pic=$object->{'Picture'};
            if($Pic==''){
                $Pic='img/ap.jpg';
            }else{
                $Pic="http://manyclicks.co.uk/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$Pic;
            }

            $Uuid=$object->{'Uuid'};
            $Addr2=$object->{'Addr2'};
            $Addr3=$object->{'Addr3'};

            if($Addr3 != '' | $Addr3 != ' '){
                $Addr2.=", ";
            }
            $BuildType=$object->{'BuildType'};
            
     

            echo '  <div class="col-md-4">
                    <article class="blog-post">
                        <figure>
                            <a href="'.$Pic.'" class="single_image">
                                <div class="blog-img-wrap">
                                    <div class="overlay">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    <img src="'.$Pic.'" alt=""/>
                                </div>
                            </a>
                            <figcaption>
                            <h3><a  class="blog-category" >'.$Addr2.$Addr3.'</a></h3>
                            <p><a href="'.$ForSaleUrl.'/propertyview.php?Uuid='.$Uuid.'" class="blog-post-title">'.$BuildType.'<i class="fa fa-angle-right"></i></a></p>
                            </figcaption>
                        </figure>
                    </article>
                </div>';

                
        }
        $x++;
    }


?>

            </div>
        </div>
    </section>
<?php include 'footer_forsale.php' ?>