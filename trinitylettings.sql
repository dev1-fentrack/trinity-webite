-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: trinitylettings
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `LandlordRegistration`
--

DROP TABLE IF EXISTS `LandlordRegistration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LandlordRegistration` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Uuid` text,
  `Email` text,
  `Title` text,
  `FirstName` varchar(100) DEFAULT NULL,
  `Surname` varchar(100) DEFAULT NULL,
  `TelephoneNumberDay` text,
  `TelephoneNumberEvening` text,
  `MobileNumber` text,
  `HowDidYouHear` text,
  `FlatNo` text,
  `BuildingName` text,
  `HouseNo` text,
  `Street` text,
  `Area` text,
  `Town` text,
  `County` text,
  `PostCode` text,
  `AdditionalMessage` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LandlordRegistration`
--

LOCK TABLES `LandlordRegistration` WRITE;
/*!40000 ALTER TABLE `LandlordRegistration` DISABLE KEYS */;
/*!40000 ALTER TABLE `LandlordRegistration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TenantRegistration`
--

DROP TABLE IF EXISTS `TenantRegistration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TenantRegistration` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Uuid` text,
  `Email` text,
  `Title` text,
  `FirstName` varchar(100) DEFAULT NULL,
  `Surname` varchar(100) DEFAULT NULL,
  `TelephoneNumberDay` text,
  `TelephoneNumberEvening` text,
  `MobileNumber` text,
  `AreaReq` text,
  `Buy` varchar(1) DEFAULT NULL,
  `PriceLow` text,
  `PriceHigh` text,
  `BedNo` text,
  `PropType` text,
  `Tenure` text,
  `ReqFrom` text,
  `Furnished` varchar(1) DEFAULT NULL,
  `Pets` varchar(1) DEFAULT NULL,
  `Smoker` varchar(1) DEFAULT NULL,
  `Children` varchar(1) DEFAULT NULL,
  `Garage` varchar(1) DEFAULT NULL,
  `OffStreetParking` varchar(1) DEFAULT NULL,
  `Modern` varchar(1) DEFAULT NULL,
  `Period` varchar(1) DEFAULT NULL,
  `RoofTerrace` varchar(1) DEFAULT NULL,
  `HowDidYouHear` text,
  `Business` text,
  `LivingStatus` text,
  `CurrentPosition` text,
  `BuyToLet` varchar(1) DEFAULT NULL,
  `ReqValuation` text,
  `MailingList` text,
  `FlatNo` text,
  `BuildingName` text,
  `HouseNo` text,
  `Street` text,
  `Area` text,
  `Town` text,
  `County` text,
  `PostCode` text,
  `AdditionalMessage` text,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TenantRegistration`
--

LOCK TABLES `TenantRegistration` WRITE;
/*!40000 ALTER TABLE `TenantRegistration` DISABLE KEYS */;
INSERT INTO `TenantRegistration` VALUES (2,'fa246d69-6492-11e6-aa4b-00163e464881','matthew.f.brown1997@gmail.com','Mr','Matthew','Brown','','','07544544806','Hipperholme','3','800','1300','3','House','Freehold','06/11/2016','Y','Y','N','N','Y','Y','Y','N','Y','Internet','Student','Students','First Time Buyer','N','N','N','','','10 ','Southedge Close','Hipperholme','Halifax','West Yorkshire','HX3 8DW','no notes');
/*!40000 ALTER TABLE `TenantRegistration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-17 15:58:11
