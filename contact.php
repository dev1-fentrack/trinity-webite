<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Contact - Trinity Sales</title>
    <link rel="stylesheet" href="required.css">
    
    <meta name="description" content="Contact Trinity Sales for your Buying or Selling needs. Independent Wakefield Estate Agency.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly & for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Contact Trinity</h1>
                        <p class="intro">Mon - Fri, 9 am - 5:30 pm<br/>Saturday 10:00 AM to 1:30 PM</p>
                </div>
            </div>
        </div>
    </section>
    <section class="search-section">
        <div class="container">
            <div class="row text-center">
                <h3>Search for a property</h3>
            </div>
            <div class="row text-center">
                <?php include 'searchLet.php' ?>
            </div>
        </div>
    </section>
    

    <section class="intro section-padding" >
        <div class="container" id="form_scroll">
            
            <div class="container">
				<h3>Welcome To Trinity Sales &amp; Lettings</h3>
				<p>586 Leeds Road<BR>Outwood<br>Wakefield, WF1 2LT<br>

				<strong>Call:</strong> <a href="tel:<?php echo $TrinityPhoneNumber; ?>"><?php echo $TrinityPhoneNumberPretty; ?></a>
				<strong>Email:</strong> <a href="mailto:<?php echo $Email; ?>"><?php echo $Email; ?></a><br></p>

				<p>If you would like us to contact you, please use the form below to submit your request and we will contact you shortly.</p>
				<p class="form_contactus_hint">Please note that items marked with a * are required.</p>

            </div>
            <div class="row">
			<div class="col-sm-5 col-md-6">
            <form action="" method="post" name="contactus" onsubmit="return contactus_validate();" class="form-modern" id="commit" name="commit">
                <div class="row form-div contact-div">
                <input type="hidden" value="ForSale" name="site" id="site">
                <input type="hidden" name="enquiry" value="General">
                <input type="hidden" name="contact" value="" >
                
                <div class="form-group text-center">
                    
                    <label class="radio-inline" for="contactcallme">
                      <input tabindex="101" class="option-input radio" type="radio" value="callme" name="contactme" id="contactcallme" checked="checked">
                      Call me: 
                    </label> 
                    <label class="radio-inline" for="contactemailme">
                      <input class="option-input radio" tabindex="102" type="radio" value="emailme" name="contactme" id="contactemailme">
                      E-mail me
                    </label>
                </div>
                <div class="form-input-group">
                    <i class="fa fa-address-book" aria-hidden="true"></i>
                    <input required="true" class="required form-control" name="fname" id="fname" tabindex="104" value="" placeholder="Forename" title="Type your first name">
                </div>

                <div class="form-input-group">
                    <i class="fa fa-address-book" aria-hidden="true"></i>
                    <input required="true" class="required form-control" name="sname" id="sname" tabindex="105" placeholder="Surname" value="" title="Type your first surname">
                </div>
                
                <div class="form-input-group">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <input required="true"  class="required  form-control" name="email" id="email" placeholder="E-mail" tabindex="106" value="" title="Type your email address">
                </div>
    
                <div class="form-input-group">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <input required="true"  class="required  form-control" name="phonew" id="phonew" placeholder="Phone" pattern="[0][0-9]{10}" tabindex="107" value="" title="Type your day time phone number">
                </div>
                
                <div class="form-input-group">
                    <i class="fa fa-sticky-note" aria-hidden="true"></i>
                    <textarea class="form-control" name="notes" id="notes" placeholder="Notes" tabindex="108" title="Type your message or comments" rows="6" cols="40"></textarea>
                </div>
                <h4>I am interested in:</h4>
                <div class="form-group row">
                    <div class="col-md-6">
                        <input class="option-input checkbox" type="checkbox" id="matchflag_furnished" tabindex="3018" title="Selling" name="selling" value="selling">
                        <label for="matchflag_furnished" title="Selling">Selling</label>
                    </div>
                    <div class="col-md-6">
                        <input class="option-input checkboxt" type="checkbox" id="matchflag_pets" tabindex="3019" title="Buying" name="buying" value="buying">
                        <label for="matchflag_pets" title="Buying">Buying</label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <input class="option-input checkbox" type="checkbox" id="matchflag_furnished" tabindex="3020" title="HMO" name="hmo" value="hmo">
                        <label for="matchflag_furnished" title="HMO">HMO</label>
                    </div>
                    <div class="col-md-6">
                        <input class="option-input checkboxt" type="checkbox" id="matchflag_pets" tabindex="3021" title="Pets" name="pets" value="pets">
                        <label for="matchflag_pets" title="Pets">Pets</label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <input class="option-input checkbox" type="checkbox" id="matchflag_furnished" tabindex="3022" title="Block Management" name="blockmanagement" value="blockmanagement">
                        <label for="matchflag_furnished" title="Block Management">Block Management</label>
                    </div>
                    <div class="col-md-6">
                        <input class="option-input checkboxt" type="checkbox" id="matchflag_pets" tabindex="3023" title="Probate" name="probate" value="probate">
                        <label for="matchflag_pets" title="Probate">Probate</label>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <input class="option-input checkbox" type="checkbox" id="matchflag_furnished" tabindex="3024" title="Divorce" name="divorce" value="divorce">
                        <label for="matchflag_furnished" title="Divorce">Divorce</label>
                    </div>
                    <div class="col-md-6">
                        <input class="option-input checkboxt" type="checkbox" id="matchflag_pets" tabindex="3019" title="Mortgages" name="mortgages" value="mortgages">
                        <label for="matchflag_pets" title="Mortgages">Mortgages</label>
                    </div>
                </div>
                <script>
                    function alertmsg() {

                        swal("Thank you", "Will will be in touch shortly");
                    }

                </script>
                </div>
                <button type="submit" tabindex="109" class="btn btn-fill btn-submit" name="submit"  value="Submit" title="Click to submit form" id="form_register_submitbutton"  onclick="if(PostRecord('ContactRequest', '#commit')){alertmsg();} return false;">Send</button>
            </form>
							
			</div>
    
			<div class="col-sm-5 col-md-6">
	            <iframe id="contact-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d382.78580950510593!2d-1.4996293469154627!3d53.7121041091461!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x10748c7944ff8d7!2sTrinity+Lettings+and+Estate+Agents!5e0!3m2!1sen!2suk!4v1470152880609" width="500" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>

	</div>				
			
    </div>
    </div>
</div>
</section>
<?php include 'footer_forsale.php' ?>