$(document).ready(function() {

	/***************** Waypoints ******************/

	$('.wp1').waypoint(function() {
		$('.wp1').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.wp2').waypoint(function() {
		$('.wp2').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.wp3').waypoint(function() {
		$('.wp3').addClass('animated fadeInRight');
	}, {
		offset: '75%'
	});

	/***************** Initiate Flexslider ******************/
	$('.flexslider').flexslider({
		animation: "slide"
	});

	/***************** Initiate Fancybox ******************/

	$('.single_image').fancybox({
		padding: 4,
	});

	/***************** Tooltips ******************/
    $('[data-toggle="tooltip"]').tooltip();

	/***************** Nav Transformicon ******************/

	/* When user clicks the Icon */
	$('.nav-toggle').click(function() {
		$(this).toggleClass('active');
		$('.header-nav').toggleClass('open');
		event.preventDefault();
	});
	/* When user clicks a link */
	

	$('.dropdown-submenu > a:not(a[href="#"])').on('click', function() {
	    self.location = $(this).attr('href');
	});

	/***************** Header BG Scroll ******************/

	$(function() {
		var logoImgWidthClass = "desktop-logo-width";
		var logoImgWidthDefault = 120;
		if ($( window ).width() <= 1024) {
			logoImgWidthClass = "mobile-logo-width";
		}
		$(window).scroll(function() {
			var scrollChange = false;
			var scroll = $(window).scrollTop();
			
			var transitionTime = "0.8s";

			if (scroll >= 20) {
				$('section.navigation').addClass('fixed');
				$('.logo-img').addClass(logoImgWidthClass);
				$('.logo-img').css({
					"-webkit-transition": transitionTime,
					" -moz-transition": transitionTime,
					"-ms-transition": transitionTime,
					"-o-transition": transitionTime,
					"transition": transitionTime,
				});
				var logoImgWidth = $(".logo-img").width();
				var navHeight = $("#index-header-nav").height();
				var navMarginLeft = $("#index-header-nav").css('margin-left');
				var logoMarginTop = navHeight-logoImgWidth;
				if (logoMarginTop <= 0) {
					logoMarginTop = 8;
				}
				$('.logo').css({
					"margin-left": parseInt(navMarginLeft,10) - 10,
					"margin-right": "15px",
					"margin-top":logoMarginTop,
					"-webkit-transition": transitionTime,
					" -moz-transition": transitionTime,
					"-ms-transition": transitionTime,
					"-o-transition": transitionTime,
					"transition": transitionTime,
				});
				
				$('#header-phone').css({
					"display" : "none",
				});
			} else {
				$('section.navigation').removeClass('fixed');
				
				$('.logo-img').removeClass(logoImgWidthClass);
				
				$('#header-phone').css({
					"display" : "block",
				});
				$('.logo').css({
					"margin-top": "8",
					"margin-left": "0",
					"margin-right": "0",
				});
			}
		});
	});
	/***************** Smooth Scrolling ******************/

	
	$(function() {

		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {

				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				

				var t=String(target.selector);

				//if (target.length && (t.indexOf("_scroll")!=-1)) {
				if ( (t.indexOf("scroll")!=-1)) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 2000);
					return false;
				}
			}
		});

	});

	$(function(){

		// $(document).ready(function(){
		// 	var phoneLeft = $('#header-phone').offset().left;
		// 	var navRight = $('.header-nav').offset().left + $('.header-nav').width();
		// 	if (phoneLeft <= navRight) {
		// 		$('#header-phone').css({
		// 			"display" : "none",
		// 		});
		// 	} else {
		// 		$('#header-phone').css({
		// 			"display" : "inline-block",
		// 		});
		// 	}
		// });
	});
	
	

		
});