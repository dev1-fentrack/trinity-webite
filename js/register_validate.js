/*
This javascript is for use with registration forms
for validating the entry of required fields
The form is expected to be called "register"
the fields validated are: email, fname, sname
*/

function register_validate(){

	if (document.register.email.value =="")
		{alert("Please Enter Your Email address!");
			register.email.focus()
		 return false;}
 
	else if (document.register.email.value.indexOf("@")==-1)
		{alert("Please Enter Your Full Email Address");
			register.email.select()
			register.email.focus()
		return false;}
	
	else if (document.register.email.value.indexOf(".")==-1)
		{alert("Please Enter Your Full Email Address");
			register.email.select()
			register.email.focus()
		return false;}		
	
	else if (document.register.fname.value =="")
		 {alert("Please Enter Your First name");
			register.fname.focus()
		 return false;}
	
	else if (document.register.sname.value =="")
		 {alert("Please Enter Your Surname");
			register.sname.focus()
		 return false;}
	 
 return true;
}

function register_onsubmit()
{
return register_validate()
}