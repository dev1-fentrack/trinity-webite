  ____       _      _       _____   ____  
 / ___|     / \    | |     | ____| / ___| 
 \___ \    / _ \   | |     |  _|   \___ \ 
  ___) |  / ___ \  | |___  | |___   ___) |
 |____/  /_/   \_\ |_____| |_____| |____/ 
                                          
++++++++++++++++++
Results.php
++++++++++++++++++
Completed: Results.php looks and feels back to normal. Notify me of any issues. Very weird what occured. Pictures no longer expand larger, they will take user to property viewing page.
Completed: Navigation runs on IF and will draw on navigation of header_tolet_results or header_forsale_results. (Results, contact and property view require variant headers).
Completed: Corrected navigation drop down CSS issue.

!No Action Needed: Result now completes the following:
---E2: Unsure how possible, but result.php should change to Trinity Sales / Lettings nav when required
---PHP should alter the headertolet.php include to header1.php include if sales.
---As the result.php will be being accessed from within the sales directory if buy is being searched, all images, css is present to display sales.

!FIXME - E3: No data-validation. Aware that the results page will pull all results, including test data, however it may be useful to include a data validation check.
Does the result have a pic? N Does the result have description and price? Y --> Show but incl 'awaiting photos'.
Does result have pic? N Description? N --> Don't show empty or uncompleted property, should any property be published as a test it shouldn't appear on results.


+++++++++++++++++++
toLet.php (Property View) Issues
+++++++++++++++++++
!FIXME - Has this been implemented to create dynamic opengraph? 
E1: Dynamic OpenGraph Data.. Attempted PHP entry in toLet.php

<meta property="og:title" content="'.$Addr1.'">
<meta property="og:site_name" content="Trinity Sales">
<meta property="og:url" content="http://manyclicks.co.uk:8081/forSale.php?Uuid=".$_GET['Uuid']""
<meta property="og:description" content="An amazing opportunity for a perfect new home. Available to purchase from Trinity Sales, an independent agent from Wakefield.">
<meta property="og:type" content="article">
<meta property="og:image" content="http://manyclicks.co.uk/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$MainPic.">
<meta property="og:image:width" content="768">
<meta property="og:image:height" content="1024">
<meta property="og:image:type" content="image/jpeg">

og:image:width & height must be defined to help Facebook. These are the actual dimensions in pixels of the image on the server. Unsure how to fetch this info.
og:image:type will also need to changed accordingly. image/png etc.

+++++++++++++++++++
searchLet.php
++++++++++++++++++
!No Action Needed I will look into this further and resolve. Work with what is present atm please
E2: Search box does not indicate present active tab (CSS issue).


Completed (by Matt): propertyview.php Assest collection file. Equal on website virtual servers. Works to ensure one single file requires edit.
Will incorporate IF statements throughout to active let / sale characteristics. (Find checkme for Connors PHP).
CSS hasn't been altered as there will be two different CSS named the same. CSS is 99% same, only hero-image address is different.


result.php
		rent search searches for sale when not clicked
	C - the result.php will now IF statement to check type and resolve
		rent header doesn't stand out from background


matt:-
	email and password input field
	make the three visible properties at the bottom be the first property searches
	contact.html:
		data input fields
	tenant.html:
		data input fields
	trinitypropertysales.com needs to be changed to this website
	rent search searches for sale when not clicked
	rent header doesn't stand out from background
	register submenu - text not visible (should now be solved)

++++++++++++++++++
footer_forsale.php
++++++++++++++++++
TODO (Connor): Needs alignment reworking. I don't get around to finishing tonight.