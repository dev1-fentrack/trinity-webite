<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Divorce - Trinity Sales</title>
    <link rel="stylesheet" href="required.css">
    
    <meta name="description" content="Contact Trinity Sales for your Buying or Selling needs. Independent Wakefield Estate Agency.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly & for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center" >
                        <h1>Divorce</h1>
                        
                        <?php include 'searchLet.php' ?>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <section class="intro section-padding">
        <div class="container">
            <div class="row" style="text-align:center;">
                <a href="contact.php#contentTop" class="btn btn-fill btn-large">Request A Divorce Valuation</a>
            </div>
            <div class="row text-center" style="margin-top:50px;">
                <div class="col-md-6">
                    <img class="intro-img" src="../img/divorce.png">
                </div>
                <div class="intro-feature col-md-6">
                    <div class="intro-content 
                        <p>Your solicitor may explain that you need to obtain a valuation for property assets in the event of divorce proceedings.</p>
                    </div>
                    <div class="intro-content 
                        <p>If this is the case a standard valuation won’t do. You’ll need a formal written report. We can help produce formal divorce valuations for you.</p>
                    </div>
                    <div class="intro-content last 
                        <p>These formal written valuations can be produced for a small administration fee which we can deduct from the selling fees when the property does come to be sold.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<?php include 'footer_forsale.php' ?>