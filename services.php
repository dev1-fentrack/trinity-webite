<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Other Services - Trinity Sales</title>
    <meta name="description" content="Here are some of our connected services you may require if you are buying or selling a property. For in depth details on our Sales services and related charges please call now on <?php echo $TrinityPhoneNumberPretty; ?> or complete this form to arrange an appraisal.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Other Services</h1>
                        <p class="intro">Some likely essential services you'll need</p>
                   
<?php include 'searchLet.php' ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="down-arrow">
                        <a href="#info_scroll" class="btn btn-fill btn-large btn-margin-right">Learn More</a></p>
        </div>
        <div class="down-arrow"> <div class="floating-arrow"><a href="#info_scroll"><i class="fa fa-angle-down"></i></a></div></div>
    </section>
    <section class="intro section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe007;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>All bidders are vetted</h5>
                        <p>We vet all potential purchasers before we contact you with their bid. We won't allow you to be strung along on an unsecure lead.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe00d;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>Shortest Contract, Best Results</h5>
                        <p>We pride ourselves on our six week contract versus industry standard of six months. We'll get your property sold quicker and for the best price.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe04e;" class="icon"></span>
                    </div>
                    <div class="intro-content last">
                        <h5>Property Portal Population</h5>
                        <p>We publish your property to all the well known property portals, included in our price. This will allow the maximum coverage for your property.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="features section-padding" id="info_scroll">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="feature-list">
                        <h3>Trinity will succeed</h3>
                        <p>Here are some of our connected services you may require if you are buying or selling a property.
                        For in depth details on our Sales services and related charges please call now on <a href="tel:<?php echo $TrinityPhoneNumber; ?>"><?php echo $TrinityPhoneNumberPretty; ?></a> or complete this form to arrange an appraisal.</p>
                        <ul class="features-stack">
                            <li class="feature-item">
                                <div class="feature-icon">
                                    <span data-icon="&#xe03e;" class="icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h5>Property Portals</h5>
                                    <p>Our team will populate the popular property portals to give your property the greatest prospective audience.</p>
                                </div>
                            </li>
                            <li class="feature-item">
                                <div class="feature-icon">
                                    <span data-icon="&#xe040;" class="icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h5>Personal Agent</h5>
                                    <p>With a simple direct point of contact, your property will be dealt with by your own personal agent.</p>
                                </div>
                            </li>
                            <li class="feature-item">
                                <div class="feature-icon">
                                    <span data-icon="&#xe03c;" class="icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h5>Revamping Estate Listings</h5>
                                    <p>We regularly review your properties progress during the sale or letting. We will revamp aspects which could be preventing a sealed deal.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
				</div>	
					
			<div class="row">
                <div class="col-sm-5 col-lg-6">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Other Services
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="problem">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="problem" data-parent="#problem" href="#problemOne">Collapsed Sale</a>
                                        </h4>
                                    </div>
                                    <div id="problemOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                        <p>Imagine the stress, cost and wasted time that a sale that breaks down will cause you! In general you can have no certainty of a sale going through up until you exchange contracts. 1 in 3 of all agreed house sales falls through - The Guardian 
                                        <b>We have a solution:</b> Wouldn’t it be great if there was more incentive for buyers and sellers to complete on a sale quickly and minimise the risk of sales collapsing at the last minute?” That’s why we have a process where you can request that both buyer & seller pay a pre-contract deposit as a sign of commitment to the sale. This way if either party does pull out, the other is compensated.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="problem" data-parent="#problem" href="#problemTwo">Mortgage Advice</a>
                                        </h4>
                                    </div>
                                    <div id="problemTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            Speak to one of our recommended independent and “whole of market” brokers who can advise you in what mortgage products are available to you and to suit your needs. 

                                            We only deal with brokers who process your application quickly, keep you informed and are “whole of market” so can offer you the best advice.

                                            Register your interest, just leave your name and number and a recommended mortgage broker will be in touch to arrange to meet you in our branch or at your home. Alternatively call us on <?php echo $TrinityPhoneNumberPretty; ?> to arrange an appointment.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="problem" data-parent="#problem" href="#problemThree">Conveyancing Services</a>
                                        </h4>
                                    </div>
                                    <div id="problemThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>When buying or selling a house your solicitor is only as good as the other solicitor they are dealing with.  This means if either the buyer or seller’s solicitor is a poor communicator or just generally slow it doesn’t matter how good yours is, it’s likely that the sale will drag unreasonably and you will be stressed and frustrated.

                                            To avoid this we recommend buyers and sellers use one of our recommended local solicitors who:<br>

                                            <strong>Act Fast</strong> – They routinely complete sales within 28 days and can do it in as few as 14 where required.<Br>
                                            <strong>Are Good Communicators</strong> – They keep both you and us informed on the process of the sale/purchase so you’re not left guessing.<br>
                                            <strong>Are Good Value</strong> – Good service doesn’t need to cost the earth.  We only work with competitively priced solicitors can offer the best value to our clients.</p>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="problem" data-parent="#problem" href="#problemFour">Probate Valuations</a>
                                        </h4>
                                    </div>
                                    <div id="problemFour" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Your solicitor may explain that you need to obtain a probate valuation for property assets left on death.  We can help with probate valuations for you.

                                            Please note we cannot accept instructions to sell a property before probate (with will) or letters of administration (without will) has been obtained. 

                                            Once obtained our revolutionary system of selling property can have the property sold within 28 days at its full market value.  Something an executor / administrator will find very useful.</p>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Financial Planning</a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>Buying a house is a big financial decision whether you are buying it alone or jointly.  Its wise to have a plan for the worst case scenario be it Death, Divorce, Bankruptcy or Illness. We recommend you speak to a Financial Planner for a FREE impartial consultation.

                                            Register your interest, just leave your name and number and we’ll be in touch with you to arrange an appointment.</p>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
            </div>
        </div>
    </section>
    <section class="hero-strip section-padding">
        <div class="container">
            <div class="col-md-12 text-center">
                <h2>
                Properties to Let
                </h2>
                <p>You may wish to view which properties we have available to let.</p>
                <a href="#" class="btn btn-ghost btn-accent btn-large">View Lettings</a>
                <div class="logo-placeholder floating-logo"><img class="img-float" src="img/house.png" alt=""></div>
            </div>
        </div>
    </section>
    <section class="blog-intro section-padding" id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Properties for Sale</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 leftcol">
                    <h5>Trinity Contract</h5>
                    <p>With the shortest estate agency contract, we know you'll be very pleased by how we treat people who are sell their house. Additionally, you will be happy with how quickly we can sell your property.</p>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 rightcol">
                    <h5>Bidder Venting</h5>
                    <p>Selling your property shouldn't be a heartache. Trinity thoroughly credit checks your potential bidders and until they are approved, we do not inform you of their bid. We ensure we only come with good news.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="blog text-center">
        <div class="container-fluid">
            <div class="row">
                <?php

    $json=file_get_contents("/GetFeaturedData.LM.v1.php?Table=ForSale");

    $data =  json_decode($json);

        $x = 0;
        foreach($data as $object){

        if($x > 0){
            $Pic=$object->{'Picture'};
            if($Pic==''){
                $Pic='img/ap.jpg';
            }else{
                $Pic="http://manyclicks.co.uk/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$Pic;
            }

            $Uuid=$object->{'Uuid'};
            $Addr2=$object->{'Addr2'};
            $Addr3=$object->{'Addr3'};

            if($Addr3 != '' | $Addr3 != ' '){
                $Addr2.=", ";
            }
            $BuildType=$object->{'BuildType'};
            
     

            echo '  <div class="col-md-4">
                    <article class="blog-post">
                        <figure>
                            <a href="'.$Pic.'" class="single_image">
                                <div class="blog-img-wrap">
                                    <div class="overlay">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    <img src="'.$Pic.'" alt=""/>
                                </div>
                            </a>
                            <figcaption>
                            <h2><a  class="blog-category" >'.$Addr2.$Addr3.'</a></h2>
                            <p><a href="'.$ForSaleUrl.'/propertyview.php?Uuid='.$Uuid.'" class="blog-post-title">'.$BuildType.'<i class="fa fa-angle-right"></i></a></p>
                            </figcaption>
                        </figure>
                    </article>
                </div>';

                
        }
        $x++;
    }


?>
                <a href="#" class="btn btn-ghost btn-accent btn-small">More properties</a>
            </div>
        </div>
    </section>
<?php include 'testimonials.php' ?>
<?php include 'footer_forsale.php' ?>