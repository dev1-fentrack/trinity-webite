<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Tenants of Trinity Lettings Properties</title>
    <meta name="description" content="Call Trinity Letting Agents on 01924 566408 for hassle free fully managed Residential Lettings and Property Management in the Leeds and Wakefield area.">
	<meta name="keywords" content="Flat to rent in Leeds, House to rent in Wakefield, Property to rent Leeds Wakefield, letting in Leeds Wakefield, letting in Leeds, lettings in Wakefield, lettings in Leeds, Lettings Agents">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Perfect lettings agent</h1>
                        <p class="intro">A completely different approach to lettings</p>
                        <a href="regtenant.php" class="btn btn-fill" >Register</a>
                    </div>
                </div>
            </div>
        </div>
    
    </section>
    <section class="search-section">
        <div class="container">
            <div class="row text-center">
                <h3>Search for a property</h3>
            </div>
            <div class="row text-center">
                <?php include 'searchLet.php' ?>
            </div>
        </div>
    </section>
    <?php include 'testimonials.php' ?>
    <section class="intro section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe033;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>Quick Registration</h5>
                        <p>All we need is your signature and a set of keys. It really is that simple! Simply complete the quick registration form and we'll be in touch!</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe030;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>Dedicated Tailored Service</h5>
                        <p>With the exact level of service you require, all your questions and needs will be met with a your own personal agent.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe046;" class="icon"></span>
                    </div>
                    <div class="intro-content last">
                        <h5>Peace of Mind</h5>
                        <p>Have peace of mind with Trinity Lettings, with guaranteed rent and eviction cover.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="features section-padding" id="services_scroll">
        <div class="container">
            <div class="row">
				<div class="container">
					<h3>Why use Trinity Lettings?</h3><br>
						<p>The lettings industry is widely regarded as having a poor reputation in terms of the service it offers both Landlords and Tenants.
						At Trinity Lettings we intend to change that. We aim to offer a professional level of service to both Landlords and Tenants. We have found from experience that treating Tenants with respect rather than contempt derives a better relationship for all parties involved.
						Tenants are encouraged to suggest how we could improve their experience and our service offered.
						Trinity Lettings offers a growing portfolio of quality accommodation covering Wakefield & Leeds. Make sure you register your details to join our mailing list and keep informed on the great properties we have available - saving you time and effort.
						No Tenancy Renewal Fees
						Tenants don't pay any renewal fees for extending their tenancy or re-signing an agreement with the Landlord.
						First class service and Maintenance Hotline
						Once you're in the property, you can leave a message on our dedicated Maintenance Hotline if you have any problems. We promise to respond within 24 hours. Our contractors will liaise with you to arrange a convenient time to conduct repairs.
						Competitive Charges
						Unlike some Agents our business strategy is not to force Tenants to pay excessive application charges to secure a property. You can secure your ideal property from only £100 which is only payable once you have decided on a property and wish to undertake the referencing procedure.</p>
					<br>
				</div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe033;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>Email Updates</h5>
                        <p>We will only email you with properties which match your perfect criteria! We hope to move you into your new home, only when you are ready!</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe030;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>Personal Agent</h5>
                        <p>With a simple direct point of contact, your letting will be aided by a dedicated agent of yours.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe046;" class="icon"></span>
                    </div>
                    <div class="intro-content last">
                        <h5>Test for Chemistry</h5>
                        <p>Romance is chemistry. We ensure you and your potential new property have a perfect chemical balance. Matching specifics, such as nearby or schools.</p>
                    </div>
                </div>
			</div>				
			
                </div>
            </div>
        </div>
    </section>
    <section class="features-extra section-padding" id="letting_scroll">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="feature-list">
                        <h3>Letting from Trinity</h3>
                        <p>If you feel ready to enquire about letting with trinity, simply register your details with us and we will contact you at the appropriate time you choose. Use our simple <a href="#register" data-toggle="tab">tenant register</a> form to quickly let us know you are interested.
						We understand for first time individuals who are seeking to let property there may be questions you wish to ask, we've created a <a href="tenantfaq.php">tenants frequently asked questions</a> list to help you establish answers quickly. Trinity is not afraid to answer any further questions you may have, in regards to our services or a property you have seen.
						Please call us on <a href="tel:<?php echo $TrinityPhoneNumber; ?>"><?php echo $TrinityPhoneNumberPretty; ?></a> during office hours for any questions. Or you can contact us via email <a href="mailto:<?php echo $Email; ?>"><?php echo $Email; ?></a>.</p>
						<a href="#" class="btn btn-fill btn-small">Contact Us</a>
						</div>
						</div>
				
				<div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tenant Services
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#applicationprocess" data-toggle="tab">Application Process</a>
                                </li>
                                <li><a href="#insurances" data-toggle="tab">Insurances</a>
                                </li>
                                <li><a href="#register" data-toggle="tab">Learn More</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="applicationprocess">
									<h3>With ease, you'll be in your new property</h3>
									<p>The application process:
									<ol>
									<li>Register your details with us and take a look at the current stock of properties we have on offer.</li>
									<li>Contact us to arrange a viewing (make sure you've registered with us first in order to save time so we can double check you fit the Landlord's requirements). We're more than happy to do evening and weekend viewings where our schedule allows.</li>
									<li>Once you've chosen your ideal property, you will need to secure it. To do this we require a payment or "application fee" which covers the referencing and administration costs. Once this has been paid the property is removed from the market. You need to complete a Tenant referencing form, which is processed through our partner Endsleigh. Simply contact us and we will email you an invoice you can settle by debit/credit card online or by BACS transfer.
									The Application fee is £100 for a single person and £50 for each additional person included in the Tenancy Agreement or for each guarantor Once we receive cleared funds we will forward the form for you to complete.
									The application fee is non refundable in all situations unless the Landlord then declines the Tenant(s) after passing the referencing procedure.
									The referencing tests examine things like: Affordability*, Employment checks, previous Landlord checks and Credit Reference checks.
									*A good way to check whether you will pass the Affordability test is by clicking on the below link and inputting your gross income. Then click "calculate" - if the figure generated is lower the advertised rent of the property you would automatically fail the test. This means you require a guarantor who will have to pass the reference test in order to secure the property.
									Affordability Test
									A Guarantor is required for applicants who fail the credit checking procedure, as well as anyone self-employed, employed less than 6 months in their current job, students, or those on a lower income.
									The Guarantor will be required to pass the Guarantor referencing test and sign a Guarantor deed confirming their willingness to act as Guarantor for the Tenant.
									Under special situations the Landlord may agree to accept Tenant(s) who fail the test, however this is at the Landlords personal discretion.</li>
									<li>On completion, we require cleared funds comprising the deposit and the first month's rent sent to our bank account - all deposits are held and paid over to the DPS for your safety and protection.</li>
									<li>On move in day, we'll meet you at the property and handover the keys at an agreed time. It may be necessary to perform an inventory of the property - you will be notified of this in advance.</li>
									<br>Please note points 4 & 5 are only relevant for "Fully Managed" properties. Potential Tenants will be informed in advance.</p>

								</div>
								
								<div class="tab-pane fade in" id="insurances">
								<h3>Insurances</h3>
								<p>Even if a property is fully furnished it's important that Tenants have some form of home contents insurance. Your Landlords insurance will NOT cover your own belongings. Any damage to the property or its contents which is caused by the Tenant's neglect is not the Landlord's repairing obligation and where the Tenant has not taken out an insurance policy it's likely that the Tenant's deposit will have to be used to cover any replacement costs.
In order to protect your deposit and your possessions we strongly recommend that Tenants take out appropriate insurances.</p>
								</div>

                                <div class="tab-pane fade in" id="register">
                                <form class="form-modern" id="commit" name="commit">
                                        <input type="hidden" value="ToLet" name="site" id="site">
                                
                                        <!-- Form Name -->
                                        <h3>Looking to know more - We'll contact you!</h3>

                                        <!-- Select Basic -->
                                        <div class="form-input-group">
                                            <i class="fa fa-address-book" aria-hidden="true"></i>
                                            <select id="title" name="title" class="form-control default-option">
                                                <option value="0">Title</option>
                                              <option value="1">Mr</option>
                                              <option value="2">Mrs</option>
                                              <option value="3">Miss</option>
                                              <option value="4">Ms</option>
                                            </select>
                                        </div>
                                        <!-- Text input-->
                                        <div class="form-input-group">
                                            <i class="fa fa-address-book" aria-hidden="true"></i>
                                            <input id="forename" name="forename" type="text" placeholder="Forename" class="form-control input-md" required="">
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-input-group">
                                            <i class="fa fa-address-book" aria-hidden="true"></i>
                                            <input id="surname" name="surname" type="text" placeholder="Surname" class="form-control input-md" required="">
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-input-group">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <input id="telephone" name="telephone" type="text" placeholder="Contact Number" pattern="[0][0-9]{10}" class="form-control input-md">
                                        </div>

                                        <!-- Multiple Radios (inline) -->
                                        <div class="form-group row text-center">
                                            <div class="col-md-4 checkdiv">
                                                <label class="control-label" for="contact">Ideal Contact Time: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <label class="radio-inline no-vertical" for="contact-0">
                                                  <input type="radio" class="option-input radio" name="contact" id="contact-0" value="1" checked="checked">
                                                  8 am - 12 pm
                                                </label> 
                                                <label class="radio-inline no-vertical" for="contact-1">
                                                  <input type="radio" class="option-input radio" name="contact" id="contact-1" value="2">
                                                  12 pm - 5 pm
                                                </label>
                                            </div>
                                         
                                            
                                         
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-input-group">
                                            <i class="fa fa-sticky-note" aria-hidden="true"></i>
                                            <textarea class="form-control" id="notes" placeholder="Notes" name="notes"></textarea>
                                        </div>

                                        <!-- Button -->
                                        <div class="form-input-group">
                                            <button id="submit" name="submit" class="btn-fill btn-submit" onclick="PostRecord('ContactRequest', '#commit'); return false;">Submit</button>
                                        </div>
                                        </form>
                                    </div>
                            </div>
						</div>
                    </div>
                        <!-- /.panel-body -->
                </div>
                    <!-- /.panel -->
            </div>
                        </p>
        </div>
         
        </div>
        </div>      
    </section>
    
    <section class="hero-strip section-padding">
        <div class="container">
            <div class="col-md-12 text-center">
                <h3 style="color:white;">
                Let - Why not buy?
                </h3>
                <p>See what properties are available to purchase</p>
                <a href="http://www.trinitypropertysales.com/" class="btn btn-ghost btn-accent btn-large">View Trinity Sales</a>
                <div class="logo-placeholder floating-logo"><img class="img-float" src="img/house.png" alt=""></div>
            </div>
        </div>
    </section>
    <section class="blog-intro section-padding" id="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Properties to Let</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12 leftcol">
                    <h5>Trinity Landlord Excellence</h5>
                    <p>Trinity deals with your landlord on your behalf to secure tenancy term, bond and payments. We endevaour to ensure the very best for yourself. Take a look at the properties available to let.</p>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 rightcol">
                    <h5>Trinity Tenancy Quality</h5>
                    <p>Trinity is aware that excellent tenants are a landlords dream. Through thorough background checks, Trinity screens all potential tenants which will allow excellent and fair pricing for both parties.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="blog text-center">
        <div class="container-fluid">
            <div class="row">
                

                <?php

    $json=file_get_contents("/GetFeaturedData.LM.v1.php?Table=ToLet");

    $data =  json_decode($json);

        $x = 0;
        foreach($data as $object){

        if($x > 0){
            $Pic=$object->{'Picture'};
            if($Pic==''){
                $Pic='img/ap.jpg';
            }else{
                $Pic="http://manyclicks.co.uk/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$Pic;
            }

            $Uuid=$object->{'Uuid'};
            $Addr2=$object->{'Addr2'};
            $Addr3=$object->{'Addr3'};

            if($Addr3 != '' | $Addr3 != ' '){
                $Addr2.=", ";
            }
            $BuildType=$object->{'BuildType'};
            
     

            echo '  <div class="col-md-4">
                    <article class="blog-post">
                        <figure>
                            <a href="'.$Pic.'" class="single_image">
                                <div class="blog-img-wrap">
                                    <div class="overlay">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    <img src="'.$Pic.'" alt=""/>
                                </div>
                            </a>
                            <figcaption>
                            <h3><a  class="blog-category" >'.$Addr2.$Addr3.'</a></h3>
                            <p><a href="'.$Url.'/propertyview.php?Uuid='.$Uuid.'" class="blog-post-title">'.$BuildType.'<i class="fa fa-angle-right"></i></a></p>
                            </figcaption>
                        </figure>
                    </article>
                </div>';

                
        }
        $x++;
    }


?>
                <a href="#" class="btn btn-fill btn-small">More properties</a>
            </div>
        </div>
    </section>
    <section class="sign-up section-padding text-center" id="signup_scroll">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h3>Trinity Landlord Newsletter</h3>
                    <p>Landlords, register your interest for the latest industry and regulation news.
					Find out how Trinity can help your earnings.</p>
                    <form class="signup-form" action="#" method="POST" role="form" id="commit2" name="commit2">
                        <input type="hidden" value="ToLet" name="site" id="site">
                        <div class="form-input-group">
                            <i class="fa fa-envelope"></i><input type="text" class="" name="email" id="email" placeholder="Enter your email" required>
                        </div>
                        <div class="form-input-group">
                            <i class="fa fa-lock"></i><input type="text" class="" name="password" id="password" placeholder="Enter your password" required>
                        </div>

                        <button type="submit" class="btn-fill sign-up-btn" name="submit"  value="Submit" title="Click to send your registration" id="form_register_submitbutton"  onclick="PostRecord('LandlordNewsletterRegistration', '#commit2'); return false;">Sign up for free</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php include 'footer_forsale.php' ?>