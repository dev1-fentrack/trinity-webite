<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Mortgages - Trinity Sales</title>
    <link rel="stylesheet" href="required.css">
    
    <meta name="description" content="Contact Trinity Sales for your Buying or Selling needs. Independent Wakefield Estate Agency.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly & for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Mortgages</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-section">
        <div class="container">
            <div class="row text-center">
                <h3>Search for a property</h3>
            </div>
            <div class="row text-center">
                <?php include 'searchLet.php' ?>
            </div>
        </div>
    </section>
    <section class="intro section-padding" >
        <div class="container">
            <div class="row">
                <div class="intro-feature">
                    <div class="col-md-6">
                        <img class="intro-img" src="../img/mortgage.jpg">
                    </div>
                    <div class="col-md-6">
                        <div class="intro-content">
                            <p>If You're a first time buyer or looking to move up/down the property ladder its important to have specialist financial advice.</p>
                        </div>
                        <div class="intro-content ">
                            <p>Going to your bank to ask for a mortgage is a false economy. We've had multipe buyers who have offered on a property only to find out that their bank wont lend as the application progresses.</p>
                        </div>
                        <div class="intro-content last ">
                            <p>Speak to one of our recommended independent and “whole of market” brokers who can advise you on what mortgage products are available to you and find a lending solution to suit your purchase.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
     <section class="features section-padding" >
        <div class="container">
            <div class="row request-div">
                <a href="contact.php#form_scroll" class="btn btn-fill btn-large">Request Information</a>
            </div>
            <div class="row" style="margin-top:50px;">
                <div class="col-md-4 intro-feature">
                    <div class="intro-content ">
                        <p>Speak to one of our recommended independent and “whole of market” brokers who can advise you in what mortgage products are available to you and to suit your needs.</p>
                    </div>
                    <div class="intro-content last ">
                        <p>We only deal with brokers who process your application quickly, keep you informed and are “whole of market” so can offer you the best advice.</p>
                    </div>
                </div>
                <div class="col-md-8 intro-feature">
                    <img class="intro-img" src="../img/mortgage2.jpg">
                </div>
            </div>
        </div>
    </section>
    
<?php include 'footer_forsale.php' ?>