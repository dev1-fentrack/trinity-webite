<?php
require_once 'db.FT.v1.php';
include 'settings.php';    


$sql='';

if($RecordType === "ToLet"){
	$sql="select  p.Id  from ".$RecordType." p left join States s on p.StateId=s.id where s.Asset='".$RecordType."' and s.Phase in ('To Let','Let Agreed','Let') order by rand() Limit 4";
} else {
	$sql="select  p.Id  from ".$RecordType." p left join States s on p.StateId=s.id where s.Asset='".$RecordType."' and s.Phase in ('For Sale', 'Sold STC','Completed') order by rand() Limit 4";
}



$stmt = $db->prepare($sql);



$stmt->execute();
$result = $stmt->fetchAll();

class Asset {}
$assets = array();


foreach($result as $row) {


	$Id=$row[0];

	$sqlImage="(select `AssetPictures`.`Image` from AssetPictures where AssetUuid=".$RecordType.".Uuid order by OrderNum desc limit 1 ) as Picture";


	if($RecordType =="ToLet"){
		$sql2="select Uuid,".$sqlImage.",Description,RentPcm,Addr1,Addr2,Addr3,BuildType From ".$RecordType." where Id=".$Id;
	} else {
		$sql2="select Uuid,".$sqlImage.",Description,Price,Addr1,Addr2,Addr3,BuildType From ".$RecordType." where Id=".$Id;
	}

	//error_log("sql2:" . preg_replace("/\s+/", " ", $sql2));

	$stmt = $db->prepare($sql2);
	$stmt->execute();
	$result2 = $stmt->fetchAll();



	foreach($result2 as $row2) {

		$e = new Asset();
		$e->Uuid=$row2[0];
		$e->Picture=$row2[1];
		$e->Description=$row2[2];
		
		if($RecordType=='ToLet'){
			$e->RentPcm=$row2[3];
		}else{
			$e->Price=$row2[3];
		}

			$e->Addr1=$row2[4];
			$e->Addr2=$row2[5];
			$e->Addr3=$row2[6];
			$e->BuildType=$row2[7];


		$assets[] = $e;
	}

	

}




header('Content-Type: application/json');
echo json_encode($assets);

?>
