<!doctype html>

<?php include 'settings.php'; ?>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />






<?php  

    
    $ForSale = $RecordType==='ForSale';

        $json=file_get_contents("http://127.0.0.1:8081/GetPropertyType.LM.v1.php?naked&Uuid=".$_GET['Uuid']);
        $data =  json_decode($json);

        foreach($data as $object){
            $type=$object->{'type'};
        }


       

        if($RecordType!=='ForSale'){
            $json=file_get_contents("http://127.0.0.1:8080/GetToLetData.LM.v1.php?naked&Uuid=".$_GET['Uuid']);
        }else{
            $json=file_get_contents("http://127.0.0.1:8081/GetForSaleData.LM.v1.php?naked&Uuid=".$_GET['Uuid']);
        } 
        

        $data =  json_decode($json);

        foreach($data as $object){


            $FloorPlanAvailable = false;
            $FloorPlan=$object->{'FloorPlan'};
            if(!$FloorPlan==''){
                $FloorPlanAvailable = true;
            }
            $MainPic=$object->{'Picture'};
            
            $FacebookPic = $Url."/GetImageForFacebook.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$MainPic;



            if($MainPic==''){
                $MainPic='img/ap.jpg';
            }else{
                $MainPic=$Url."/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$MainPic;
            }


            $Title=$object->{'Title'};
            

            $Addr1=$object->{'Addr1'};

            $AddrLatLongAvailable = false;
            $AddrLatLong=$object->{'AddrLatLong'};
            if(!$AddrLatLong == ''){
                $LLArray=explode(',', $AddrLatLong);
                $AddrLatLongAvailable = true;
            }

            $Phase=$object->{'Phase'};
            //error_log("Phase: ".$Phase);

            $STCString = '';

            if($type === "ForSale"){
                if($Phase == "Sold STC"){
                    $STCString = "<h3  style='color:red;'>Sold STC</h3>";
                }
                if($Phase == "Completed"){
                    $STCString = "<h3  style='color:red;'>Sold</h3>";
                }
            } else {
                if($Phase == "Let Agreed"){
                    $STCString = "<h3  style='color:red;'>Let Agreed</h3>";
                }
                if($Phase == "Let"){
                    $STCString = "<h3  style='color:red;'>Let</h3>";
                }
            }

        }
        echo '<title>'.$Title.'</title>'; 
        //error_log("stc string:".$STCString);

        echo '<link rel="stylesheet" href="required.css">';
            

            if($ForSale) {
                echo '
                <title>Buy with Confidence - Trinity</title>
                <meta name="description" content="Trinity Sales, Local Estate Agents in Wakefield, Pontefract, Ossett and Castleford. Find out how we can sell your property in 28 days.">
                <meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly & for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="apple-touch-icon" href="apple-touch-icon.png">
                <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
                <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
                <link rel="stylesheet" href="css/normalize.min.css">
                <link rel="stylesheet" href="css/bootstrap.min.css">
                <link rel="stylesheet" href="css/jquery.fancybox.css">
                <link rel="stylesheet" href="css/flexslider.css">
                <link rel="stylesheet" href="css/styles-propview.css">
                <link rel="stylesheet" href="css/queries.css">
                <link rel="stylesheet" href="css/etline-font.css">
                <link rel="stylesheet" href="css/forms.css">
                <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
                <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>';

            }else{ //trinity lettings heading!
                echo '
                <title>Rent with Confidence - Trinity</title>
                <meta name="description" content="Trinity Lettings.">
                <meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly & for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="apple-touch-icon" href="apple-touch-icon.png">
                <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
                <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
                <link rel="stylesheet" href="css/normalize.min.css">
                <link rel="stylesheet" href="css/bootstrap.min.css">
                <link rel="stylesheet" href="css/jquery.fancybox.css">
                <link rel="stylesheet" href="css/flexslider.css">
                <link rel="stylesheet" href="css/styles-propview.css">
                <link rel="stylesheet" href="css/queries.css">
                <link rel="stylesheet" href="css/etline-font.css">
                <link rel="stylesheet" href="css/forms.css">
                <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
                <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>';
            }
        echo '

                    <!-- You can use Open Graph tags to customize link previews.
            Learn more: https://developers.facebook.com/docs/sharing/webmasters -->

        <meta property="fb:app_id" content="1140939032660559" />
        <meta property="og:title" content="'.$Title.'" /> 
        <meta property="og:site_name" content="Trinity Sales" />
        <meta property="og:url" content="'.$Url.'/propertyview.php?Uuid='.$_GET["Uuid"].'" />
        <meta property="og:description" content="An amazing opportunity for a perfect new home. Available to purchase from Trinity Sales, an independent agent from Wakefield." >
        <meta property="og:type"   content="website" />
        <meta property="og:image" content="'.$FacebookPic.'" />
        <meta property="og:image:width" content="768" />
        <meta property="og:image:height" content="1024" />
        <meta property="og:image:type" content="image/jpeg" />


            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
            <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
            
             <!-- jQuery -->
            <script src="./bower_components/jquery/dist/jquery.min.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
            
            <script src="./bower_components/bootstrap/js/tab.js"></script>
            

            <!-- Slider -->
            <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
            <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
            <!-- use jssor.slider.debug.js instead for debug -->
            <script>
                jQuery(document).ready(function ($) {
                    
                    var jssor_1_SlideshowTransitions = [
                      {$Duration:1200,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,x:-0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,y:0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,y:-0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,y:-0.3,$During:{$Top:[0.3,0.7]},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,y:0.3,$SlideOut:true,$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,x:0.3,$Cols:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,x:0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,y:0.3,$Rows:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,y:0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,y:0.3,$Cols:2,$During:{$Top:[0.3,0.7]},$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,y:-0.3,$Cols:2,$SlideOut:true,$ChessMode:{$Column:12},$Easing:{$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,x:0.3,$Rows:2,$During:{$Left:[0.3,0.7]},$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,x:-0.3,$Rows:2,$SlideOut:true,$ChessMode:{$Row:3},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,x:0.3,y:0.3,$Cols:2,$Rows:2,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$ChessMode:{$Column:3,$Row:12},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,$Delay:20,$Clip:3,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,$Delay:20,$Clip:3,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,$Delay:20,$Clip:12,$Assembly:260,$Easing:{$Clip:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                      {$Duration:1200,$Delay:20,$Clip:12,$SlideOut:true,$Assembly:260,$Easing:{$Clip:$Jease$.$OutCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
                    ];
                    
                    var jssor_1_options = {
                      $AutoPlay: true,
                      $SlideshowOptions: {
                        $Class: $JssorSlideshowRunner$,
                        $Transitions: jssor_1_SlideshowTransitions,
                        $TransitionsOrder: 1
                      },
                      $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$
                      },
                      $ThumbnailNavigatorOptions: {
                        $Class: $JssorThumbnailNavigator$,
                        $Cols: 10,
                        $SpacingX: 8,
                        $SpacingY: 8,
                        $Align: 360
                      }
                    };
                    
                    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
                    
                    //responsive code begin
                    //you can remove responsive code if you dont want the slider scales while window resizing
                    function ScaleSlider() {
                        var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                        if (refSize) {
                            refSize = Math.min(refSize, 800);
                            jssor_1_slider.$ScaleWidth(refSize);
                        }
                        else {
                            window.setTimeout(ScaleSlider, 30);
                        }
                    }
                    ScaleSlider();
                    $(window).bind("load", ScaleSlider);
                    $(window).bind("resize", ScaleSlider);
                    $(window).bind("orientationchange", ScaleSlider);
                    //responsive code end
                });
            </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

            </head>
        <body id="top">
            

            <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->
            <section class="hero">
                <section class="navigation">
                    <header>
                        <div class="header-content">';

         //checkme IF statement to check type = let, then display let website header and navigation.

        if($ForSale){
        echo '
                        <div class="logo"><a href="'.$Url.'"><img src="'.$Image.'" width="100 px" height="69 px" alt="Trinity Lettings logo"></a></div>
                        <div class="header-nav">
                            <nav>
                                <ul class="primary-nav dropdown">
                                    <li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                                    <li><a href="index.php#first_scroll">Buying</a></li>
                                    <li><a href="selling.php">Selling</a></li>
                                    <li><a href="services.php">Services</a></li>
                                    <li><a href="contact.php">Contact</a></li>
                                    <li id="fat-menu" class="dropdown">
                                      <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">Register<b class="caret"></b></a>
                                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                                        <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regvendor.php">Register to Sell</a></li>
                                        <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regbuyer.php">Register to Buy</a></li>
                                      </ul>
                                    </li>
                                    <li><a href="'.$Url.'">'.$Name.'</a></li>
                                    <li><a href="'.$Facebook.'" target="_blank" rel="nofollow"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="'.$Twitter.'" target="_blank" rel="nofollow"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="mailto:'.$Email.'"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>

                                </ul>
                                <ul class="member-actions">
                                    <li><a href="tel:'.$TrinityPhoneNumber.'" class="btn-white btn-small">'.$TrinityPhoneNumberPretty.'</a></li>
                                </ul>
                            </nav>';
    }else{
        //checkme must include this section and logo could change for lettings in future.
        echo '
                        <div class="logo"><a href="'.$Url.'"><img src="'.$Image.'" width="100 px" height="69 px" alt="Trinity Sales logo"></a></div>
                        <div class="header-nav">
                            <nav>
                                <ul class="primary-nav dropdown">
                                    <li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                                    <li><a href="index.php#scroll_landlord">Landlords</a></li>
                                    <li><a href="tenant.php">Tenants</a></li>
                                    <li><a href="contact.php">Contact</a></li>
                                    <li id="fat-menu" class="dropdown">
                                      <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">Register<b class="caret"></b></a>
                                      <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                                        <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="reglandlord.php">Become a Landlord</a></li>
                                        <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regtenant.php">Become a Tenant</a></li>
                                      </ul>
                                    </li>
                                    <li><a href="propertymanagement.php">Management</a></li>
                                    <li><a href="index.php#scroll_services">Services</a></li>
                                    <li><a href="'.$Url.'">'.$Name.'</a></li>
                                    <li><a href="'.$Facebook.'" target="_blank" rel="nofollow"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="'.$Twitter.'" target="_blank" rel="nofollow"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="mailto:'.$Email.'"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                                          </ul>
                                        </li>
                                </ul>
                                <ul class="member-actions">
                                    <li><a href="tel:'.$TrinityPhoneNumber.'" class="btn-white btn-small">'.$TrinityPhoneNumberPretty.'</a></li>
                                </ul>
                            </nav>
            ';


    } ?>  

                                </div>
                                <div class="navicon">
                                    <a class="nav-toggle" href="#"><span></span></a>
                                </div>
                            </div>
                        </header>
                    </section>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="hero-content text-center">
                                <h1><font color="black"><?php echo $Title; ?></font></h1>
                                     <p class="intro"></p>
                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="down-arrow floating-arrow"><a href="#contentTop"><i class="fa fa-angle-down"></i></a></div>
     </section>

    <section class="features">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-offset-4">
                    <div class="descButton buttonAttribute">
                            <a class="textButton textButtonA textButtonB textButtonD" role="button" href="#gallery">Photos</a>
                            <a class="textButton textButtonA textButtonB textButtonD" role="button" href="#details">Details</a>
    <?php
        if($FloorPlanAvailable){
            echo '<a class="textButton textButtonA textButtonB textButtonD" data-toggle="collapse" data-parent="#accordion" onclick="javascript:location.href=\'#furtherinfo\'" href="#collapseOne">Floor Plan</a>';
        }

        if($AddrLatLongAvailable){
            echo '                         <a class="textButton textButtonA textButtonB textButtonD" role="button" onclick="javascript:location.href=\'#furtherinfo\'" href="#collapseTwo">Location (Map)</a>
                                <a class="textButton textButtonA textButtonB textButtonD" role="button" onclick="javascript:location.href=\'#furtherinfo\'" href="#collapseThree">Street View</a>';
        }

    ?>
   
                            <a class="textButton textButtonA textButtonB textButtonD" role="button" onclick="history.go(-1); return false;">Return to Results</a>
                    </div>
                </div>
        </div>
    </div>
</section>
    <section class="intro">
        <div class="container" id="gallery contentTop">
            <div class="row" style="background-color: white; width: 100%;">
                <div class="col-lg-12">
                    
                    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 800px; height: 556px; overflow: hidden; visibility: hidden;">
                    <!-- Loading Screen -->
                    <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                        <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                        <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                    </div>
                    <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 800px; height: 456px; overflow: hidden;">


                    <?php
                    if(!$ForSale){
                        $json=file_get_contents("http://127.0.0.1:8080/GetToLetData.LM.v1.php?naked&Uuid=".$_GET['Uuid']);
                      } else {
                        $json=file_get_contents("http://127.0.0.1:8081/GetForSaleData.LM.v1.php?naked&Uuid=".$_GET['Uuid']);
                      }
                        $data =  json_decode($json);

                        foreach($data as $object){

                            $MainPic=$object->{'Picture'};
                            if($MainPic==''){
                                $MainPic='img/ap.jpg';
                            }else{
                                $MainPic=$Url."/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$MainPic;
                            }
                            
                            echo'
                                <div data-p="144.50" style="display: none;">
                                <img data-u="image" src="'.$MainPic.'" />
                                <img data-u="thumb" src="'.$MainPic.'" height="72px" width="72px" />
                                </div>
                                ';
                        }

                        $json=file_get_contents("http://127.0.0.1:8081/GetAssetImages.LM.v1.php?naked&Uuid=".$_GET['Uuid']);

                        $data =  json_decode($json);

                        foreach($data as $object){

                            $Pic=$object->{'Image'};
                            if($Pic==''){
                                $Pic='img/ap.jpg';
                            }else{
                                $Pic=$Url."/GetImage.v1.php?naked&c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$Pic;
                            }

                            echo'
                                <div data-p="144.50" style="display: none;">
                                <img data-u="image" src="'.$Pic.'" />
                                <img data-u="thumb" src="'.$Pic.'" height="72px" width="72px" />
                                </div>
                                ';

                        }


                    ?>


                   
                    </div>

                    <!-- Thumbnail Navigator -->
                    <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
                        <!-- Thumbnail Item Skin Begin -->
                        <div data-u="slides" style="cursor: default;">
                            <div data-u="prototype" class="p">
                                <div class="w">
                                    <div data-u="thumbnailtemplate" class="t"></div>
                                </div>
                                <div class="c"></div>
                            </div>
                        </div>
                        <!-- Thumbnail Item Skin End -->
                    </div>
                    <!-- Arrow Navigator -->
                    <span data-u="arrowleft" class="jssora05l" style="top:200px;left:8px;width:40px;height:40px;"></span>
                    <span data-u="arrowright" class="jssora05r" style="top:200px;right:8px;width:40px;height:40px;"></span>

                </div>
            </div>
            </div>
        </div>
</section>
<br>
<section class="features">
        <div class="container" style="padding: 10px;" id="details">
        
        <div class="row" id="share">

Share this property
<!-- Facebook -->

<?php



if($type == "ForSale"){
    echo '<a href="https://www.facebook.com/sharer/sharer.php?u='.$Url.'/propertyview.php?Uuid='.$_GET['Uuid'].'" target="_blank" class="share-btn facebookshare">
            <i class="fa fa-facebook" style="font-size:24px;"></i>
        </a>';

    echo '<a href="http://twitter.com/share?url='.$Url.'/propertyview.php?Uuid='.$_GET['Uuid'].'&text=Wow%21+Check+out+this+amazing+property+from+@Sales_Trinity+%23forsale+%23dreamhome" target="_blank" class="share-btn twitter">
            <i class="fa fa-twitter" style="font-size:24px;"></i>
        </a>
';

    echo "<a href=\"mailto:?subject=I've%20found%20an%20amazing%20property&body=Trinity%20Sales%20has%20a%20property%20for%20sale%20and%20instantly%20I%20thought%20of%20you!%0A%0A".$Url."/propertyview.php?Uuid=".$_GET['Uuid']."\" target='_blank' class='share-btn email'>
            <i class='fa fa-envelope' style='font-size:24px;'></i>
        </a>";
} else {
    echo '<a href="https://www.facebook.com/sharer/sharer.php?u='.$Url.'/propertyview.php?Uuid='.$_GET['Uuid'].'" target="_blank" class="share-btn facebookshare">
            <i class="fa fa-facebook" style="font-size:24px;"></i>
        </a>';

    echo '<a href="http://twitter.com/share?url='.$Url.'/propertyview.php?Uuid='.$_GET['Uuid'].'&text=Wow%21+Check+out+this+amazing+property+from+@Sales_Trinity+%23forsale+%23dreamhome" target="_blank" class="share-btn twitter"><!--FIXME Needs PHP URL encoding, take the URL of the page including UID to share this property page -->
            <i class="fa fa-twitter" style="font-size:24px;"></i>
        </a>
';

     echo "<a href=\"mailto:?subject=I've%20found%20an%20amazing%20property&body=Trinity%20Sales%20has%20a%20property%20for%20sale%20and%20instantly%20I%20thought%20of%20you!%0A%0A".$Url."/propertyview.php?Uuid=".$_GET['Uuid']."\" target='_blank' class='share-btn email'>
            <i class='fa fa-envelope' style='font-size:24px;'></i>
        </a>";
}


?>
        </div>

        <div class="row">
        <?php

        if(!$ForSale){
               $json=file_get_contents("http://127.0.0.1:8080/GetToLetData.LM.v1.php?naked&Uuid=".$_GET['Uuid']);
                $data =  json_decode($json);


                foreach($data as $object){

                    $Title=$object->{'Title'};
                    $Addr1=$object->{'Addr1'};
                    $Addr2=$object->{'Addr2'};
                    $PostCode=$object->{'PostCode'};
                    $Price=$object->{'RentPcm'};


                    $FeatureList = (trim($object->{'FeatureList'})!='')?preg_split("/\r\n|\n|\r/", trim($object->{'FeatureList'})):null;

                    $Description = $object->{'Description'};
                }
                echo '
                        <div class="col-md-8">
                        <div class="row">
                        <div class="col-md-6"><h4>'.$Title.' '.$PostCode.'</h4></div>
                        <div class="col-md-6"><h5>Rent PCM £'.$Price.'</h5>.'.$STCString.'</div>
                        </div>

                    ';
            }else{
                $json=file_get_contents("http://127.0.0.1:8081/GetForSaleData.LM.v1.php?Uuid=".$_GET['Uuid']);
                $data =  json_decode($json);


                foreach($data as $object){

                    $Addr1=$object->{'Addr1'};
                    $Addr2=$object->{'Addr2'};
                    $PostCode=$object->{'PostCode'};
                    $Price=$object->{'Price'};
                    $Title=$object->{'Title'};

                    echo '
                         <div class="col-md-8">
                        <div class="row"><font color="black">
                        <div class="col-md-6"><h4>'.$Title.', '.$PostCode.'</h4></div>
                        <div class="col-md-6"><h5>Asking £'.$Price.'</h5>.'.$STCString.'</div>
                        </font></div>

                    ';

                    $FeatureList = (trim($object->{'FeatureList'})!='')?preg_split("/\r\n|\n|\r/", trim($object->{'FeatureList'})):null;

                    $Description = $object->{'Description'};
                }
            }


                echo '<ul2><font size="3pt" color="black">';

                $isLeft = true;
                foreach ($FeatureList as $feature) {
                    if($isLeft){
                        echo '<li class="li-left">'.$feature.'</li>';
                        $isLeft = false;
                    } else {
                        echo '<li class="li-right">'.$feature.'</li>';
                        $isLeft = true;
                    }
                    
                }

                if(!$isLeft){
                    echo '<ui class="li-right" style="height:20px;"> </ui>';

                }

                echo '</font></ul2>
                <script>
                function ShowDetails(){
                    document.getElementById("details_button").style.display = "none";
                    document.getElementById("descText").style.display = "";
                }
                </script>';
                echo '<div class="col-md-6">';
                        
                if(strlen($Description)>0){
                    echo '<br><br><button class="textButton textButtonA"  title="More Information" id="details_button"  onclick="ShowDetails();return false;">More information</button>';
                }
                echo '</div>';
                
                echo "<br> <span style=\"display:none;\" id=\"descText\" >".$Description."</span>";

        ?>
               
                
                    
                
                        
                </div>
                <div class="col-md-4 well" style="background-color: white;">
                
                <form class="form-horizontal" id="commit" name="commit">

                <?php
                    if(!$ForSale){
                        echo '<input type="hidden" value="ToLet" name="site" id="site">';
                    } else {
                        echo '<input type="hidden" value="ForSale" name="site" id="site">';
                    }

                ?>
                                
                                        <fieldset>

                                        <!-- Form Name -->
                                        <h4>Arrange a Viewing</h4>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="title">Title</label>
                                          <div class="col-md-9">
                                            <select id="title" name="title" class="form-control required">
                                              <option value="Mr">Mr</option>
                                              <option value="Mrs">Mrs</option>
                                              <option value="Miss">Miss</option>
                                              <option value="Ms">Ms</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="fname">Forename</label>  
                                          <div class="col-md-9">
                                          <input id="fname" name="fname" type="text" placeholder="Forename" class="form-control input-md required" required="true">
                                            
                                          </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="sname">Surname</label>  
                                          <div class="col-md-9">
                                          <input id="sname" name="sname" type="text" placeholder="Surname" class="form-control input-md required" required="true">
                                            
                                          </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="phonew">Contact Number</label>  
                                          <div class="col-md-9">
                                          <input id="phonew" name="phonew" type="text" placeholder="01XXXXXXXXX" pattern="[0][0-9]{10}" class="form-control input-md required" required="true">
                                            
                                          </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="phonew">Email</label>  
                                          <div class="col-md-9">
                                          <input id="email" name="email" type="text" placeholder="me@isp.com" class="form-control input-md required" required="true">
                                            
                                          </div>
                                        </div>

                                        <!-- Multiple Radios (inline) -->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="contact">Ideal Contact Time</label>
                                          <div class="col-md-9"> 
                                            <label class="radio-inline" for="contact-0">
                                              <input type="radio" name="contact" id="contact-0" value="8 am - 12 pm" checked="checked">
                                              8 am - 12 pm
                                            </label> 
                                            <label class="radio-inline" for="contact-1">
                                              <input type="radio" name="contact" id="contact-1" value="12 pm - 5 pm">
                                              12 pm - 5 pm
                                            </label>
                                          </div>
                                        </div><br>

                                        <!-- Textarea -->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="textarea">Notes</label>
                                          <div class="col-md-9">                     
                                            <textarea class="form-control" id="notes" name="notes" placeholder="Is there availability to view this property on Saturday?"></textarea>
                                          </div>
    
                                            <input id="property" name="property" type="hidden" value= <?php echo '"'.$_GET['Uuid'].'"'; ?> >
                                            
                                        </div>

                                        <!-- Button -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="submit"></label>
                                          <div class="col-md-4">

                                            <script>
                                                function alertmsg() {

                                                    swal("Thank you", "Confirmation of a visit will be given after talking with landlord/owner.");
                                                }

                                            </script>


                                            <button id="submit" name="submit" class="btn btn-fill" onclick="if(PostRecord('ViewingRequest', '#commit')){ alertmsg()} return false;">Submit</button>
                                          </div>
                                        </div>

                                        </fieldset>
                                        </form>
                </div>
                </div>
                <div class="col-lg-12 descButton buttonAttribute">
                        <a class="textButton textButtonA textButtonB textButtonD" role="button" href="#gallery">Photos</a>
                            <a class="textButton textButtonA textButtonB textButtonD" role="button" href="#details">Details</a>
    <?php
        if($FloorPlanAvailable){
            echo '<a class="textButton textButtonA textButtonB textButtonD" data-toggle="collapse" data-parent="#accordion" onclick="javascript:location.href=\'#furtherinfo\'" href="#collapseOne">Floor Plan</a>';
        }


        if($AddrLatLongAvailable){
            echo'                             <a class="textButton textButtonA textButtonB textButtonD" role="button" onclick="javascript:location.href=\'#furtherinfo\'" href="#collapseTwo">Location (Map)</a>
                                <a class="textButton textButtonA textButtonB textButtonD" role="button" onclick="javascript:location.href=\'#furtherinfo\'" href="#collapseThree">Street View</a>';
        }

    ?>


                            <a class="textButton textButtonA textButtonB textButtonD" role="button" onclick="goBack();">Return to Results</a>
                </div>
                

    </section>

<section class="intro section-padding" id="furtherinfo">
        <div class="container">
            <div class="row">
            <h3>Further Property Information</h3>
            <div class="panel-default">
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                

<?php

    if($FloorPlanAvailable){
        $FloorPlan=$Url."/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$FloorPlan;

                                echo '<div class="panel panel-default">
                                    <div data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="panel-heading"  onmouseover="" style="cursor: pointer;">
                                        <h4 class="panel-title">
                                            Floor Plan
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        <img src="'.$FloorPlan.'" height="100%" width="100%">
                                        </div>
                                    </div>
                                </div>';
                            }


    if($AddrLatLongAvailable){
        echo '                                <div class="panel panel-default">
                                    <div data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="panel-heading" onmouseover="" style="cursor: pointer;">
                                        <h4 class="panel-title">
                                            Location (Map)
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">';

                                            echo '<iframe width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='.$LLArray[0].'+'.$LLArray[1].'&hl=es;z=14&output=embed"></iframe><br /><small>
                                            <a href="https://maps.google.com/maps?q='.$AddrLatLong.'&hl=es;z=14&amp;output=embed" style="color:#0000FF;text-align:left"></a>';

    }
?>
                                            </iframe>
                                        </div>
                                    </div>
                                </div>


<?php

    if($AddrLatLongAvailable){
        echo'
                                        <div class="panel panel-default">
                                            <div data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="panel-heading" onmouseover="" style="cursor: pointer;">
                                                <h4 class="panel-title">
                                                    Street View
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://my.ctrlq.org/maps/#street|1|69.93488499990701|-3.51983014682551|'.$LLArray[0].'|'.$LLArray[1].'"></iframe>';
    }
?>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                    </div>
                    <div class="col-md-10 col-md-offset-1"><p><strong>Important Notice:</strong>
Descriptions of the property are subjective and are used in good faith as an opinion and NOT as a statement of fact. Please make further specific enquires to ensure that our descriptions are likely to match any expectations you may have of the property. We have not tested any services, systems or appliances at this property. We strongly recommend that all the information we provide be verified by you on inspection, and by your Surveyor and Conveyancer. In addition, services such as maps and street view are provided by Google and are copyright of such.</p></div>
                </div>               
            </div>
        
    </section>

<?php
    if($RecordType==='ForSale'){
        include 'footer_forsale.php';
    }else{
        include 'footer_tolet.php';
    }

?>