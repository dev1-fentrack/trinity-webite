<?php
require_once 'db.FT.v1.php';
include 'settings.php';    

$Table=$_GET['Table'];

$sql='';

$SearchCriteria = " ";

if(isset($_GET['Location'])){
    $SearchCriteria.=' AND (Addr2="'.$_GET['Location'].'" OR Addr3="'.$_GET['Location'].'" OR Addr4="'.$_GET['Location'].'")';
}

if(isset($_GET['SaleMinPrice'])){
	$MinPrice = preg_replace("/[^0-9]/","",$_GET['SaleMinPrice']);
	if( $MinPrice != "0"){
	    $SearchCriteria.=' AND Price>= CONVERT('.$MinPrice.', SIGNED INTEGER) ';
	}
} elseif(isset($_GET['LetMinPrice'])){
	$MinPrice = preg_replace("/[^0-9]/","",$_GET['LetMinPrice']);
	if( $MinPrice != "0"){
	    $SearchCriteria.=' AND RentPcm>= CONVERT('.$MinPrice.', SIGNED INTEGER) ';
	}
}

if(isset($_GET['SaleMaxPrice'])){
	$MaxPrice = preg_replace("/[^0-9]/","",$_GET['SaleMaxPrice']);
	if( $MaxPrice != "0"){
	    $SearchCriteria.=' AND Price<= CONVERT('.$MaxPrice.', SIGNED INTEGER) ';
	}
} elseif(isset($_GET['LetMaxPrice'])){
	$MaxPrice = preg_replace("/[^0-9]/","",$_GET['LetMaxPrice']);
	if( $MaxPrice != "0"){
	    $SearchCriteria.=' AND RentPcm<= CONVERT('.$MaxPrice.', SIGNED INTEGER) ';
	}
}

if(isset($_GET['NumBeds'])){
	if($_GET['NumBeds'] == "5+"){

		$NumBeds = preg_replace("/[^0-9]/","",$_GET['NumBeds']);
		$SearchCriteria.=' AND Bedrooms>="'.$NumBeds.'"';

	} elseif($_GET['NumBeds'] != "Any"){

	    $SearchCriteria.=' AND Bedrooms="'.$_GET['NumBeds'].'"';
	}
}

if(isset($_GET['PropType'])){
    $SearchCriteria.=' AND BuildType="'.$_GET['PropType'].'"';
}



$sqlImage="(select `AssetPictures`.`Image` from AssetPictures where AssetUuid=a.Uuid order by OrderNum desc limit 1 ) as Picture";

if($Table=='ToLet'){
	$sql="select a.Uuid,".$sqlImage.",a.Description,a.RentPcm,a.Addr1,a.Addr2,a.PostCode,s.Phase,a.FeatureList From ToLet a left join States s on a.StateId=s.Id where  s.Asset='ToLet' and s.Phase in ('To Let','Let Agreed','Let') AND a.deleted='N' ".$SearchCriteria;
	$sql.= "ORDER BY CAST(a.RentPcm AS decimal(6,2))";
}

if($Table=='ForSale'){
	$sql="select a.Uuid,".$sqlImage.",a.Description,a.Price,a.Addr1,a.Addr2,a.PostCode,s.Phase,a.FeatureList From ForSale a left join States s on a.StateId=s.Id where  s.Asset='ForSale' and s.Phase in ('For Sale','Sold STC','Completed') AND a.deleted='N' ".$SearchCriteria;
	$sql.= "ORDER BY CAST(a.Price AS decimal(6,2))";
}

error_log(">>>>>>>>>>>>>sql:".$sql);
error_log("here5:");

if($sql==''){
	die(-1);
}

//error_log("sql:" . preg_replace("/\s+/", " ", $sql));

$stmt = $db->prepare($sql);


error_log("here3:");

$stmt->execute();
error_log("here4:");

$result = $stmt->fetchAll();

class Asset {}
$assets = array();

foreach($result as $row) {
	//error_log('del row');
	$e = new Asset();
	$e->Uuid=$row[0];
	$e->Picture=$row[1];
	$e->Description=$row[2];
	
	if($Table=='ToLet'){
		$e->RentPcm=$row[3];
	}else{
		$e->Price=$row[3];
	}

		$e->Addr1=$row[4];
		$e->Addr2=$row[5];
		$e->PostCode=$row[6];

		$e->Phase=$row[7];
		$e->FeatureList=$row[8];


	$assets[] = $e;
}

error_log("here2:");

header('Content-Type: application/json');
error_log("here22:");
echo json_encode($assets);
error_log("here222:");

?>