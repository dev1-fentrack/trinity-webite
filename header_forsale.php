<!-- Header One Sales
Used on: Index, Selling, Services, Register to Sell, Register to Buy
-->

<?php include 'settings.php' ?>

<link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" href="required.css">
    <link rel="stylesheet" href="css/normalize.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="css/radiocss/radio.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/etline-font.css">
    <link rel="stylesheet" href="bower_components/animate.css/animate.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/magnific.css">

    <!-- <script src="./js/magnific.js"></script> -->
    <script src="sweetalert-master/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
    
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <script src="js/search.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	 <!-- jQuery -->
    <script src="./bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	
	<script src="./bower_components/bootstrap/js/tab.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

	</head>
<body id="top">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <section class="hero">
        <section class="navigation">
            <header class="site-header">
                <div class="header-content">
                    <div class="site-header-wrap">  
                        <div class="logo"><a href="<?php echo $Url; ?>"><img class="logo-img" src="<?php echo $ToLetImage = $Image;; ?>"  alt="Trinity Lettings logo"></a>
                        </div>
                        <div id="header-btn" class="header-vertical">
                            <a href="contact.php#down_scroll" class="btn btn-fill" rel="nofollow" onClick="fbq('track', 'SmartVal Button');">Book a Free Valuation</a>
                        </div>
                        <div id="header-phone" class="header-vertical">
                            <span class="branch-tel"><a href="tel:01924609811">Phone: <?=$TrinityPhoneNumberPretty?></a></span> 
                            <a id="menu-toggle" class="menu-button" href="#"><span class="icon-menu"></span></a>
                        </div>
                    </div>
                    <div class="navicon">
                        <a class="nav-toggle" href="#"><span></span></a>
                    </div>
                 </div>
            </header>
            <header id="index-header-nav">
                <div class="header-nav">
                    <nav>
                        <ul class="primary-nav dropdown multi-level">
                            <li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                            <li class="dropdown-submenu">
                                <a href="#" id="drop4" role="button" class="dropdown-toggle" data-toggle="dropdown">Sales<b class="caret"></b></a>
                                <ul class="dropdown-menu dropdown-mobile" role="menu" aria-labelledby="drop4">
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="selling.php">Selling</a></li>
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="buying.php">Buying</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a href="#" id="drop5" role="button" class="dropdown-toggle" data-toggle="dropdown">Lettings<b class="caret"></b></a>
                                <ul class="dropdown-menu dropdown-mobile" role="menu" aria-labelledby="drop5">
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="landlord.php">Landlords</a></li>
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="tenant.php">Tenants</a></li>
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="hmo.php">HMO</a></li>
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="blockmanagement.php">Block Management</a></li>
                                </ul>
                            </li> 
                            <li><a href="probate.php">Probate</a></li>
                            <li><a href="divorce.php">Divorce</a></li>
                            <li><a href="mortgages.php">mortgages</a></li>
                            <li><a href="https://www.facebook.com/pg/sell.my.house.in.wakefield/notes/" target="_blank">Blog</a></li>
                            <li><a href="contact.php">Contact</a></li>
                            <li  class="dropdown">
                                <a href="#" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">Register<b class="caret"></b></a>
                                <ul class="dropdown-menu dropdown-mobile" role="menu" aria-labelledby="drop3">
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="reglandlord.php">Landlord Registration</a></li>
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regtenant.php">Tenant Registration</a></li>
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regvendor.php">Seller Registration</a></li>
                                    <li role="presentation"><a style="color: #b2cc33;" role="menuitem" tabindex="-1" href="regbuyer.php">Buyer Registration</a></li>
                              </ul>
                            </li>
                            <li><a href="<?php echo $Facebook; ?>" target="_blank" rel="nofollow"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="<?php echo $Twitter; ?>" target="_blank" rel="nofollow"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="mailto:<?php echo $Email; ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>

                        </ul>
                    </nav>
                </div>
            </header>
        </section>