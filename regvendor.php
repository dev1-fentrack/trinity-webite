<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Sell with Trinity Sales</title>
    <link rel="stylesheet" href="required.css">

    <meta name="description" content="Trinity Sales, Local Estate Agents in Wakefield, Pontefract, Ossett and Castleford. Find out how we can sell your property in 28 days.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly & for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>

        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Register Your Property</h1>
                        <p class="intro">Let Trinity sell your property, promptly, properly and professionally.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="search-section">
        <div class="container">
            <div class="row text-center">
                <h3>Search for a property</h3>
            </div>
            <div class="row text-center">
                <?php include 'searchLet.php' ?>
            </div>
        </div>
    </section>
    

    <section class="intro section-padding" >
        <div class="container">
            <div class="row">
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe033;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>No Commitments</h5>
                        <p>Trinity operate very differently to other agencies. Fundamentally, we don't tie you into 6 month contracts, we have contracts for 6 weeks.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe030;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>Dedicated Tailored Service</h5>
                        <p>With a personal agent, your property will be put to market and have one call-to point. Meaning our agent knows and deals with every potential bidder for your property. They learn what works best for you and ensures you are happy.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe046;" class="icon"></span>
                    </div>
                    <div class="intro-content last">
                        <h5>Peace of Mind</h5>
                        <p>Trinity Sales handles all the business with very little effort required by yourself. Giving you chance to enjoy the last moments in your current home.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features section-padding">
        <div class="container">
            <div class="row">
            	<h3>What makes Trinity Lettings Different?</h3>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe033;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>Fewer Voids</h5>
                        <p>By not charging you tenants re-sign fees we make it easy for your tenants to stay longer in your properties.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe030;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>Tenants Treated With Respect</h5>
                        <p>We don’t rip off or ignore your tenants because we know their far more likely to pay the rent on time and look after your property that way.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe046;" class="icon"></span>
                    </div>
                    <div class="intro-content last">
                        <h5>Easy Management</h5>
                        <p>We tailor our management to what works well for you. We can either handle everything and just keep you updated or use your preferred contractors etc when something needs fixing.</p>
                    </div>
                </div>
            </div><div id="down_scroll"></div>
        </div>
    </section>
    <section class="intro section-padding">
        <div class="container">
        <h3>Vendor Registration Form</h3>
            <div class="row">
				

				<div id="form_register">
                <form class="form-modern" id="commit" name="commit">
                    <div class='row form-div'>
					<div class="col-sm-5 col-md-6">
                        <h4>Your Contact Details</h4>
                        <div class="form-input-group">
                            <i class="fa fa-address-book" aria-hidden="true"></i>
                            <select class="form-control default-option" tabindex="3000" class="required" required="true"  title="title" name="title" id="title">
                            <option value="" selected="selected">Title</option>
                            <option value="Mr">Mr</option>
                            <option value="Mrs">Mrs</option>
                            <option value="Ms">Ms</option>
                            <option value="Miss">Miss</option>
                            <option value="Mr &amp; Mrs">Mr &amp; Mrs</option>
                            <option value="Dr">Dr</option>
                            <option value="Prof">Prof</option>
                            <option value="Dame">Dame</option>
                            <option value="Sir">Sir</option>
                            <option value="Lady">Lady</option>
                            <option value="Lord">Lord</option>
                            <option value="Father">Father</option>
                            <option value="Rev">Rev</option>
                            <option value="Rt Hon">Rt Hon</option>
                            <option value="Sister">Sister</option>
                            </select>
                        </div>
						<div class="form-input-group">
                            <i class="fa fa-address-book" aria-hidden="true"></i>
                            <input class="form-control" type="text" tabindex="3004" placeholder="First Name" required="true" name="fname" id="fname" value="" size="15" maxlength="40" title="Fill in your first name. This is a required field.">
                        </div>  
                        
                        <div class="form-input-group">
                            <i class="fa fa-address-book" aria-hidden="true"></i>
                            <input class="form-control" type="text" tabindex="3005" placeholder="Surname"  required="true" name="sname" id="sname" value="" size="25" maxlength="40" title="Fill in your lastname/surname. This is a required field.">
                        </div>
                            
                        <div class="form-input-group">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <input class="form-control" type="text" tabindex="3006" placeholder="Phone No. (Day)" required="true" name="phonew" id="phonew" pattern="[0][0-9]{10}" value="" size="35" maxlength="25" title="Fill in your phone for contact by phone. This is your most common contact number mainly your daytime telephone number. Providing your international dial code is also handy">
                        </div>

                        
                        <div class="form-input-group">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <input class="form-control" type="text" tabindex="3008" name="mobile" placeholder="Phone No. (Mobile)" id="mobile" value="" size="35" pattern="[0][0-9]{10}" maxlength="25" title="Fill in your mobile/cellular for on the road. Providing your international dial code is also handy. This number is handy for calling when you are not at your day time number">
                        </div>
                        <div class="form-input-group">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <input class="form-control" type="text" required="true" tabindex="3009" name="email" id="email" value="" size="35" maxlength="250" placeholder="E-mail" title="Fill in your email address for a response. This is a required field in the form yourname@yourwesite.com">
                        </div>
					</div>
                    <div class="col-sm-5 col-md-6">
                        <div class="form-group row">
                            <div class="col-md-8 checkdiv">
                                <label class="form-check-label" for="valuation" title="Would you like us to value your property for sale?">Would you like a valuation of your property?</label>
                            </div>
                            <div class="col-md-4">
                                <input class="option-input checkbox" type="checkbox" tabindex="3033" name="valuation" id="valuation" value="valuation" title="Would you like us to value your property for sale?">
                            </div>
                        </div>
                        <h4>Home Address</h4>
                        <div class="form-input-group">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <input class="form-control" type="text" tabindex="3016" placeholder="Property Number or Name" name="address0" id="address0" value="" size="25" maxlength="40" title="Fill in your building or house name/number">
                        </div>
                        <div class="form-input-group">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <input class="form-control" type="text" tabindex="3017" placeholder="Street Name" name="address3" id="address3" value="" size="25" maxlength="60" title="Fill in your street name">
                        </div>
                        <div class="form-input-group">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <input class="form-control" type="text" tabindex="3018" placeholder="Town/City" name="address5" id="address5" value="" size="35" maxlength="60" title="Fill in your Town or City">
                        </div>
                        <div class="form-input-group">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <input class="form-control" type="text" tabindex="3019" placeholder="County" name="address6" id="address6" value="" size="35" maxlength="60" title="Fill in your county">
                        </div>
                        <div class="form-input-group">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <input class="form-control" type="text" tabindex="3020" placeholder="Postcode" name="postcode" id="postcode" value="" size="15" maxlength="12" title="Fill in your postal code">
                        </div>
                        <input class="form-control" type="hidden" value="ForSale" name="site" id="site">
                        <h4>Additional Message, Notes or Comments</h4>
                        <div class="form-input-group">
                            <i class="fa fa-sticky-note" aria-hidden="true"></i>
                            <textarea tabindex="3020" name="notes" id="notes" placeholder="Notes" class="form_register_textarea form-control" title="Type in any additional Message, Notes or Comments" rows="5" cols="70"></textarea>
                        </div>
                       
            
					</div>        
					<script>
					    function alertmsg() {

					        swal("Thank you", "Will will be in touch shortly");
					    }

  					</script>
  					</div>
            <div class="row">
              <div class="col-md-6 submit-div">
                 <button type="submit" name="submit"  value="Submit" title="Click to send your registration" id="form_register_submitbutton" class="btn-fill btn-submit"  onclick="if(PostRecord('VendorRegistration', '#commit')){alertmsg();} return false;">Submit</button>
              </div>
            </div>
                            
					</form>
          </div>
								
				
			    </div>
	    	</div>
	</section>	
<?php include 'footer_forsale.php' ?>