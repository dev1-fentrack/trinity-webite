<?php
require_once ('db_trinitylettings.php');


$DatabaseName = "trinitylettings";

$con = Db::connect();
Db::open($DatabaseName, $con);

global $RelatedTable;


if(!isset($RelatedTable)){
	$RelatedTable = "LandlordRegistration";
}

/*
foreach($_POST as $key=>$value)
{
 error_log($key."=".$value);
}
*/


$message=null;
//error_log("SaveRegistration");
//error_log($RelatedTable);



try {
	if($RelatedTable == 'LandlordRegistration'){
		$sql = "INSERT INTO ContactRequest(Uuid, Site, Email,Title,FirstName,Surname,TelephoneNumberDay,TelephoneNumberEvening,MobileNumber,HowDidYouHear,FlatNo,BuildingName,HouseNo,Street,Area,Town,County,PostCode,AdditionalMessage, Form) VALUES 
		(
			(select UUID()),
			'".mysql_real_escape_string($_POST['site'])."',
			'".mysql_real_escape_string($_POST['email'])."',
			'".mysql_real_escape_string($_POST['title'])."',
			'".mysql_real_escape_string($_POST['fname'])."',
			'".mysql_real_escape_string($_POST['sname'])."',
			'".mysql_real_escape_string($_POST['phonew'])."',
			'".mysql_real_escape_string($_POST['phoneh'])."',
			'".mysql_real_escape_string($_POST['mobile'])."',
			'".mysql_real_escape_string($_POST['source'])."',
			'".mysql_real_escape_string($_POST['address2'])."',
			'".mysql_real_escape_string($_POST['address0'])."',					
			'".mysql_real_escape_string($_POST['address1'])."',
			'".mysql_real_escape_string($_POST['address3'])."',
			'".mysql_real_escape_string($_POST['address4'])."',
			'".mysql_real_escape_string($_POST['address5'])."',
			'".mysql_real_escape_string($_POST['address6'])."',
			'".mysql_real_escape_string($_POST['postcode'])."',
			'".mysql_real_escape_string($_POST['notes'])."',
			'".$RelatedTable."')";
		} else if ($RelatedTable == 'TenantRegistration'){
			$sql = "INSERT INTO ContactRequest(Uuid, Site, Email,Title,FirstName,Surname,TelephoneNumberDay,TelephoneNumberEvening,MobileNumber,AreaReq,

			PriceLow,PriceHigh,BedNo,PropType,Tenure, ReqFrom,Furnished,Pets,Smoker,Children,

			Garage,OffStreetParking,Modern,Period,RoofTerrace,HowDidYouHear,Business,LivingStatus,CurrentPosition,

			ReqValuation,MailingList,FlatNo,BuildingName,HouseNo,Street,Area,Town,County,PostCode,

				AdditionalMessage, Form ) VALUES 
		(
			(select UUID()),
			'".mysql_real_escape_string($_POST['site'])."',
			'".mysql_real_escape_string($_POST['email'])."',
			'".mysql_real_escape_string($_POST['title'])."',
			'".mysql_real_escape_string($_POST['fname'])."',
			'".mysql_real_escape_string($_POST['sname'])."',
			'".mysql_real_escape_string($_POST['phonew'])."',
			'".mysql_real_escape_string($_POST['phoneh'])."',
			'".mysql_real_escape_string($_POST['mobile'])."',
			'".mysql_real_escape_string($_POST['otherareas'])."',

			'".mysql_real_escape_string($_POST['pricelow'])."',					
			'".mysql_real_escape_string($_POST['pricehigh'])."',
			'".mysql_real_escape_string($_POST['propbedr'])."',
			'".mysql_real_escape_string($_POST['proptype'])."',
			'".mysql_real_escape_string($_POST['proptenum'])."',
			'".mysql_real_escape_string($_POST['movedate'])."',";
			$sql.=(array_key_exists ( 'furnished' , $_POST )?"'Y'":"'N'").",";
			$sql.=(array_key_exists ( 'pets' , $_POST )?"'Y'":"'N'").",";
			$sql.=(array_key_exists ( 'smoker' , $_POST )?"'Y'":"'N'").",";
			$sql.=(array_key_exists ( 'children' , $_POST )?"'Y'":"'N'").",";

			$sql.=(array_key_exists ( 'garden' , $_POST )?"'Y'":"'N'").",";
			$sql.=(array_key_exists ( 'offstreetparking' , $_POST )?"'Y'":"'N'").",";
			$sql.=(array_key_exists ( 'modern' , $_POST )?"'Y'":"'N'").",";
			$sql.=(array_key_exists ( 'period' , $_POST )?"'Y'":"'N'").",";
			$sql.=(array_key_exists ( 'roofterrace' , $_POST )?"'Y'":"'N'").",";
			$sql.="'".mysql_real_escape_string($_POST['source'])."',
			'".mysql_real_escape_string($_POST['business'])."',
			'".mysql_real_escape_string($_POST['persont'])."',
			'".mysql_real_escape_string($_POST['ownhome'])."',";

			$sql.=(array_key_exists ( 'valuation' , $_POST )?"'Y'":"'N'").",";
			$sql.=(array_key_exists ( 'mailinglist' , $_POST )?"'Y'":"'N'").",";
			$sql.="'".mysql_real_escape_string($_POST['address2'])."',
			'".mysql_real_escape_string($_POST['address0'])."',				
			'".mysql_real_escape_string($_POST['address1'])."',
			'".mysql_real_escape_string($_POST['address3'])."',
			'".mysql_real_escape_string($_POST['address4'])."',
			'".mysql_real_escape_string($_POST['address5'])."',
			'".mysql_real_escape_string($_POST['address6'])."',
			'".mysql_real_escape_string($_POST['postcode'])."',

			'".mysql_real_escape_string($_POST['notes'])."',
			'".$RelatedTable."'".
		")";
		} else if($RelatedTable == 'LandlordNewsletterRegistration'){
			$sql = "INSERT INTO ContactRequest(Uuid,FirstName, Site, Email, Password, Form) VALUES (	
			(select UUID()),
			'".mysql_real_escape_string($_POST['email'])."',
			'".mysql_real_escape_string($_POST['site'])."',
			'".mysql_real_escape_string($_POST['email'])."',
			'".mysql_real_escape_string($_POST['password'])."',
			'".$RelatedTable."')";
		} else if($RelatedTable == 'ContactRequest'){
			$sql = "INSERT INTO ContactRequest(Uuid, ContactMe, Site, Title, FirstName, Surname, Email, TelephoneNumberDay, Enquiry, Contact, AdditionalMessage, Form) VALUES (
			(select UUID()),
			'".mysql_real_escape_string($_POST['contactme'])."',
			'".mysql_real_escape_string($_POST['site'])."',
			'".mysql_real_escape_string($_POST['title'])."',
			'".mysql_real_escape_string($_POST['fname'])."',
			'".mysql_real_escape_string($_POST['sname'])."',
			'".mysql_real_escape_string($_POST['email'])."',
			'".mysql_real_escape_string($_POST['phonew'])."',
			'".mysql_real_escape_string($_POST['enquiry'])."',
			'".mysql_real_escape_string($_POST['contact'])."',
			'".mysql_real_escape_string($_POST['notes'])."',
			'".$RelatedTable."'".
		")";
		}else if($RelatedTable == 'ViewingRequest'){
			$sql = "INSERT INTO ContactRequest(Uuid, Site, Title, FirstName, Surname, Email,TelephoneNumberDay, Contact, AdditionalMessage, Form,PropertyUuid) VALUES (
			(select UUID()),
			'".mysql_real_escape_string($_POST['site'])."',
			'".mysql_real_escape_string($_POST['title'])."',
			'".mysql_real_escape_string($_POST['fname'])."',
			'".mysql_real_escape_string($_POST['sname'])."',
			'".mysql_real_escape_string($_POST['email'])."',
			'".mysql_real_escape_string($_POST['phonew'])."',
			'".mysql_real_escape_string($_POST['contact'])."',
			'".mysql_real_escape_string($_POST['notes'])."',
			'".$RelatedTable."',
			'".mysql_real_escape_string($_POST['property'])."'".
		")";
		} else if ($RelatedTable == 'BuyerRegistration'){
			$sql = "INSERT INTO ContactRequest(Uuid, Site, Email,Title,FirstName,Surname,TelephoneNumberDay,TelephoneNumberEvening,MobileNumber,

			FlatNo,BuildingName,HouseNo,Street,Area,Town,County,PostCode,

				AdditionalMessage, Form ) VALUES 
		(
			(select UUID()),
			'".mysql_real_escape_string($_POST['site'])."',
			'".mysql_real_escape_string($_POST['email'])."',
			'".mysql_real_escape_string($_POST['title'])."',
			'".mysql_real_escape_string($_POST['fname'])."',
			'".mysql_real_escape_string($_POST['sname'])."',
			'".mysql_real_escape_string($_POST['phonew'])."',
			'".mysql_real_escape_string($_POST['phoneh'])."',
			'".mysql_real_escape_string($_POST['mobile'])."',

			";


			$sql.="'".mysql_real_escape_string($_POST['address2'])."',
			'".mysql_real_escape_string($_POST['address0'])."',				
			'".mysql_real_escape_string($_POST['address1'])."',
			'".mysql_real_escape_string($_POST['address3'])."',
			'".mysql_real_escape_string($_POST['address4'])."',
			'".mysql_real_escape_string($_POST['address5'])."',
			'".mysql_real_escape_string($_POST['address6'])."',
			'".mysql_real_escape_string($_POST['postcode'])."',

			'".mysql_real_escape_string($_POST['notes'])."',
			'".$RelatedTable."'".
		")";
		} else if($RelatedTable == 'VendorRegistration'){
				$sql = "INSERT INTO ContactRequest(Uuid, Site, Email,Title,FirstName,Surname,TelephoneNumberDay,TelephoneNumberEvening,MobileNumber,

				FlatNo,BuildingName,HouseNo,Street,Area,Town,County,PostCode,

					AdditionalMessage, Form ) VALUES 
			(
				(select UUID()),
				'".mysql_real_escape_string($_POST['site'])."',
				'".mysql_real_escape_string($_POST['email'])."',
				'".mysql_real_escape_string($_POST['title'])."',
				'".mysql_real_escape_string($_POST['fname'])."',
				'".mysql_real_escape_string($_POST['sname'])."',
				'".mysql_real_escape_string($_POST['phonew'])."',
				'".mysql_real_escape_string($_POST['phoneh'])."',
				'".mysql_real_escape_string($_POST['mobile'])."',

				";


				$sql.="'".mysql_real_escape_string($_POST['address2'])."',
				'".mysql_real_escape_string($_POST['address0'])."',				
				'".mysql_real_escape_string($_POST['address1'])."',
				'".mysql_real_escape_string($_POST['address3'])."',
				'".mysql_real_escape_string($_POST['address4'])."',
				'".mysql_real_escape_string($_POST['address5'])."',
				'".mysql_real_escape_string($_POST['address6'])."',
				'".mysql_real_escape_string($_POST['postcode'])."',

				'".mysql_real_escape_string($_POST['notes'])."',
				'".$RelatedTable."'".
			")";
		}




	
			error_log("sql:" . preg_replace("/\s+/", " ", $sql));

			// use exec() because no results are returned
			$result=mysql_query($sql,$con);

}
catch(PDOException $e){
    $message=$sql . "<br>" . $e->getMessage();
    error_log("sql error:$message");
}



header('Content-Type: application/json');


if($message!=null){
	echo json_encode(array('status' => 'error','message'=> $message));

}else{
	echo json_encode(array('status' => 'success','message'=> 'saved'));
}
?>
