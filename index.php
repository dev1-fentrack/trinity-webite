<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Welcome To Trinity Sales &amp; Lettings</title>
    <meta name="description" content="Trinity Sales, Local Estate Agents in Wakefield, Pontefract, Ossett and Castleford. Find out how we can sell your property in 28 days.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="hero-content text-center">
                    <h1>Welcome To Trinity Sales &amp; Lettings</h1>
                    <p class="intro"><a href="request_appraisal.php" class="btn btn-fill" >BOOK YOUR FREE APPRAISAL NOW</a></p>
                    

<!-- <?php
    $json=file_get_contents("http://127.0.0.1:8081/GetFeaturedData.LM.v1.php?Table=ForSale");

    $data =  json_decode($json);

    

    if($data[0] != ""){
        $Pic=$data[0]->{'Picture'};
        if($Pic==''){
            $Pic='img/ap.jpg';
        }else{
            $Pic="GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$Pic;
        }

        $Uuid=$data[0]->{'Uuid'};
 


        echo '
                            <div class="col-md-3" style="border-radius: 15px; margin-bottom: 5px; padding: 2px; display: block; background-color: #c9db70;">
                                <h4 style="color: white;">Featured Property</h4>

                                <img src="'.$Pic.'" width="100%" height="100%" /><br>
                                <a href="'.$Url.'/propertyview.php?Uuid='.$Uuid.'" class="btn btn-fill">View Property</a>
                            </div>';
    }
    



?> -->

                    
                </div>
    </section><!-- Closes incl header section -->
<section class="contact-section">
    <div class="container">
        <div class="row text-center">
            <a href="contact.php" class="btn btn-fill">Press to go to contacts</a>
        </div>
    </div>
</section>
<section class="search-section">
    <div class="container">
        <div class="row text-center">
            <h3>Search for a property</h3>
        </div>
        <div class="row text-center">
            <?php include 'searchLet.php' ?>
        </div>
    </div>
</section>
<?php include 'testimonials.php' ?>
<section style="background-color: #F3F4F8">
        <div class="container">
            <iframe style="display: block; margin: 30px auto;" frameborder="0" width="560" height="315" src="https://biteable.com/watch/embed/25-chichester-street-copy-1496614/481a008ddab11c5a33fb7d44a5ec98232ded16fb" allowfullscreen="true"></iframe><p class="text-center"><a href="https://biteable.com/watch/25-chichester-street-copy-1496614/481a008ddab11c5a33fb7d44a5ec98232ded16fb">25 Chichester Street (copy)</a> on <a href="https://biteable.com">Biteable</a>.</p>
        </div> 
<section class="intro section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe033;" class="icon"></span>
                    </div>
                    <div class="intro-content" id="first_scroll">
                        <h5>Quick Registration</h5>
                        <p>All we need is your signature and a set of keys. It really is that simple! Simply complete the quick registration form and we'll be in touch!</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe030;" class="icon"></span>
                    </div>
                    <div class="intro-content">
                        <h5>Dedicated Tailored Service</h5>
                        <p>With the exact level of service you require, all your questions and needs will be met with a your own personal agent.</p>
                    </div>
                </div>
                <div class="col-md-4 intro-feature">
                    <div class="intro-icon">
                        <span data-icon="&#xe046;" class="icon"></span>
                    </div>
                    <div class="intro-content last">
                        <h5>Peace of Mind</h5>
                        <p>Have peace of mind with Trinity Lettings, with guaranteed rent and eviction cover.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    </section>
<section class="features section-padding" id="scroll_services">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="feature-list">
                        <h3>Trinity will succeed</h3>
                        <p>We will present your property for sale or lettings to a high standard. Gain a lot of attention and receive excellent pricing for your property.</p>
                        <ul class="features-stack">
                            <li class="feature-item">
                                <div class="feature-icon">
                                    <span data-icon="&#xe03e;" class="icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h5>Property Portals</h5>
                                    <p>Our team will populate the popular property portals to give your property the greatest prospective audience.</p>
                                </div>
                            </li>
                            <li class="feature-item">
                                <div class="feature-icon">
                                    <span data-icon="&#xe040;" class="icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h5>Personal Agent</h5>
                                    <p>With a simple direct point of contact, your property will be dealt with by your own personal agent.</p>
                                </div>
                            </li>
                            <li class="feature-item">
                                <div class="feature-icon">
                                    <span data-icon="&#xe03c;" class="icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h5>Revamping Estate Listings</h5>
                                    <p>We regularly review your properties progress during the sale or letting. We will revamp aspects which could be preventing a sealed deal.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>  
                    
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Trinity Services
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#home" data-toggle="tab">Property Management</a>
                                </li>
                                <li><a href="#homeown" data-toggle="tab">Home Owner</a>
                                </li>
                                <li><a href="#tenfind" data-toggle="tab">Tenant Find</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="home">
                                    <h4>Property Management</h4>
                                    <p>Trinity Lettings take the stress and hassle out of letting & managing a property. Remove the headaches and have us manage your property & tenants for you. Learn more about Fully Managed Properties.<br><br>Remove all the risks, get guaranteed rent & legal cover included with your property management for complete peace of mind. Learn more about Fully Managed & Protected Properties</p>
                                </div>
                                <div class="tab-pane fade" id="homeown">
                                    <h4>Home Owner</h4>
                                    <p>This service is tailored to accidental and first time landlords who are letting a property as a temporary strategy. Typically for one of the following reasons:
                                    <ul type="square">
                                        <li>Your preferred choice is to sell but your property is in negative equity so renting is a good way to cover the mortgage while the market recovers</li>
                                        <li>You are moving out of the area and don't want to sell your house or leave it empty</li>
                                        <li>You have bought another house and have not sold your old house hence it is going to be empty and costing money</li>
                                        <li>A property you are responsible for has become empty maybe due to death or illness and renting for the short term is a better option than selling immediately</li>
                                        <br>Deciding to rent a family home can be a stressful decision, luckily with our experience of managing these situations it doesn't have to be.
                                        Letting your property with our Home Owner Package, you'll get:
                                        <li>Free rental appraisal by our lettings manager as well as any advice on what can be done to maximise the rent you achieve and minimise the time it takes to let</li>
                                        <li>Your property marketed to 97% of the online renter market via Rightmove, Zoopla, Primelocation etc. and to the registered house hunters on our database</li>
                                        <li>Accompanied viewings with our friendly viewing agents</li>
                                        <li>Feedback from viewings, along with any subsequent offers from tenants</li>
                                        <li>Comprehensive reference checking of prospective tenants, with thorough employment & previous landlord references as well as the standard credit and CCJ checks.</li>
                                        <li>Bullet proof protection from a comprehensive inventory and filmed schedule of condition so in the unlikely event that your property isn't returned in its original state there is documentary evidence to support your case</li>
                                        <li>The bespoke Trinity Lettings tenancy agreement designed to offer you maximum protection</li>
                                        <li>A thorough move-in brief with your new tenants where we give them an individual house guide to your property to help them settle in as well as helping them register the move with utility suppliers and the local council.</li>
                                        <li>Any deposits lodged with a government approved scheme and administered for the life of the tenancy</li>
                                        <li>24h maintenance reporting line for tenants</li>
                                        <li>Repairs handling tailored to your preference, i.e. We handle everything / We handle some things but not others / You handle everything</li>
                                        <li>Proactive rent collection and monitoring along with a monthly statement of account which you can use for your tax records</li>
                                        <li>Periodic inspections of your property for reassurance and to spot any developing issues</li>
                                        <li>Annual property appraisal meeting with our office manager, at your request, where we will help you with any plans for the future such as selling or letting for the longer term</li>
                                        <br>Plus you can get added help with:
                                        <li>Arranging any certificates you may need like Gas, Electrical & EPC's</li>
                                        <li>Handling any repairs or upgrades prior to letting - use our recommended tradesmen who will do the works and can invoice you direct, our business model is not to make margin on landlord repairs</li>
                                        <li>Referrals into our recommended landlord insurance brokers who can help you with ensuring you've got the right level of cover for your needs and for the best price</li>
                                        <li>Dealing with your mortgage company - most lenders will ask for a rental report form an ARLA accredited agent and submission of further paperwork once let. We'll handle all this for you</li>
                                        <li>Advice on selling; our sister company Trinity Sales Estate Agents can help advise you on how best to sell your property for its full market value and fast, along with practical advice on what home improvements you may consider to maximise your sale price achieved.</li>
                                        </u>
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="tenfind">
                                    <h4>Tenant Find</h4>
                                    <p>If you're a professional Landlord and are comfortable with dealing with the management jobs that being a landlord involves (handling queries from tenants, arranging repairs, keeping on top of rent collection and changes in legislation etc.) you may find our Tenant Find service is best for your needs.
                                    With our Tenant Find service you'll get:
                                    <ul type="square">
                                    <li>Free rental appraisal by our lettings manager as well as any advice on what can be done to maximise the rent you achieve and minimise the time it takes to let
                                    <li>Your property marketed to 97% of the online renter market via Rightmove, Zoopla, Primelocation etc. and to the registered house hunters on our database
                                    <li>Accompanied viewings with our friendly viewing agents
                                    <li>Feedback from viewings, along with any subsequent offers from tenants
                                    <li>Comprehensive reference checking of prospective tenants, with thorough employment & previous landlord references as well as the standard credit and CCJ checks.
                                    <br>Plus you can get added help with:
                                    <li>A free rental review on any of your other rental properties
                                    <li>Arranging any certificates you may need like Gas, Electrical & EPC's
                                    <li>Handling any repairs or upgrades prior to letting - use our recommended tradesmen who will do the works and can invoice you direct, our business model is not to make margin on landlord repairs
                                    <li>Referrals into our recommended landlord insurance brokers who can help you with ensuring you've got the right level of cover for your needs and for the best price
                                    <li>Protecting the deposit, Move Ins, Move Outs and liaising with your mortgage provider if necessary.
                                    </ul>
                                    <br>
                                    "I use Trinity whenever one of my tenants gives notice. They're always very efficient and helpful. It helps me keep my voids to a minimum without having the hassle and upfront cost of advertising in the paper or online" Mrs C - Landlord Wakefield

                                    To find out whether the Tenant Find service is right for your needs call us today on <a href="<?php echo $TrinityPhoneNumber; ?>"><?php echo $TrinityPhoneNumberPretty; ?></a> and we'll discuss your situation and advise you accordingly.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
            </div>
            <!-- /.row -->          
            
                </div>
            </div>
        </div>
    </section>
<div id="down_scroll" style="margin-bottom: 50px"></div>

    <section class="blog text-center">
        <div class="container-fluid">
            <div class="row">

<?php

    $json=file_get_contents("http://127.0.0.1:8081/GetFeaturedData.LM.v1.php?Table=ForSale");

    $data =  json_decode($json);

        $x = 0;
        foreach($data as $object){

        if($x > 0){
            $Pic=$object->{'Picture'};
            if($Pic==''){
                $Pic='img/ap.jpg';
            }else{
                $Pic="GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$Pic;
            }

            $Uuid=$object->{'Uuid'};
            $Addr2=$object->{'Addr2'};
            $Addr3=$object->{'Addr3'};

            if($Addr3 != '' | $Addr3 != ' '){
                $Addr2.=", ";
            }
            $BuildType=$object->{'BuildType'};
            
     

            echo '  <div class="col-md-4">
                    <article class="blog-post">
                        <figure>
                            <a href="'.$Pic.'" class="single_image">
                                <div class="blog-img-wrap">
                                    <div class="overlay">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    <img src="'.$Pic.'" alt=""/>
                                </div>
                            </a>
                            <figcaption>
                            <h3><a  class="blog-category" >'.$Addr2.$Addr3.'</a></h3>
                            <p><a href="'.$Url.'/propertyview.php?Uuid='.$Uuid.'" class="blog-post-title">'.$BuildType.'<i class="fa fa-angle-right"></i></a></p>
                            </figcaption>
                        </figure>
                    </article>
                </div>';

                
        }
        $x++;
    }


?>



                <a href="result.php?type=ForSale&SaleMinPrice=0&SaleMaxPrice=0&SaleNumBeds=Any%20Beds" class="btn btn-fill">More properties</a>
            </div>
        </div>
    </section>
<?php include 'footer_forsale.php' ?>