<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Estate Agents for West Yorkshire - Trinity Sales</title>
    <meta name="description" content="Sell in days not months for the market price. Call Trinity Sales Estate Agents Wakefield on 01924 609811 to sell in 28 days.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Trinity Estate Agents</h1>
                        <p class="intro">West Yorkshire Estate Agents with a difference</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section class="features-extra section-padding" id="landlord_scroll">
        <div class="container">
            <div class="row">

                <div  class="text-center">
                <h3>Request an appraisal</h3>
                    <form class="form-horizontal" id="commit" name="commit">
                        <input type="hidden" value="callme" name="contactme" id="contactcallme">
                        <input type="hidden" value="ForSale" name="site" id="site">
                        <fieldset>
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="title">Title</label>
                                          <div class="col-md-9">
                                            <select id="title" name="title" class="form-control">
                                              <option value="Mr">Mr</option>
                                              <option value="Mrs">Mrs</option>
                                              <option value="Miss">Miss</option>
                                              <option value="Ms">Ms</option>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="forename">Forename</label>  
                                          <div class="col-md-9">
                                          <input id="fname" name="fname" type="text" placeholder="Forename" class="form-control input-md" required="">
                                            
                                          </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="surname">Surname</label>  
                                          <div class="col-md-9">
                                          <input id="sname" name="sname" type="text" placeholder="Surname" class="form-control input-md" required="">
                                            
                                          </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="telephone">Telephone</label>  
                                          <div class="col-md-9">
                                          <input id="phonew" name="phonew" type="text" placeholder="+44" pattern="[0][0-9]{10}" class="form-control input-md">
                                            
                                          </div>
                                        </div>

                                        <input id="email" name="email" class="form-control input-md" type="hidden">

                                        <!-- Reason -->
                                        <label class="col-md-3 control-label" for="enquire">Enquiry</label>
                                          <div class="col-md-9">
                                            <select id="enquiry" name="enquiry" class="form-control">
                                              <option value="General">General</option>
                                              <option value="Visit">Visit</option>
                                              <option value="Offer">Offer</option>
                                              <option value="Register">Register</option>
                                            </select>
                                          </div>

                                        <!-- Multiple Radios (inline) -->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="contact">Contact Time</label>
                                          <div class="col-md-9"> 
                                            <label class="radio-inline" for="contact-0">
                                              <input type="radio" name="contact" id="contact-0" value="8 am - 12 pm" checked="checked">
                                              8 am - 12 pm
                                            </label> 
                                            <label class="radio-inline" for="contact-1">
                                              <input type="radio" name="contact" id="contact-1" value="12 pm - 5 pm">
                                              12 pm - 5 pm
                                            </label>
                                          </div>
                                        </div><br>

                                        <!-- Textarea -->
                                        <div class="form-group">
                                          <label class="col-md-3 control-label" for="textarea">Notes</label>
                                          <div class="col-md-9">                     
                                            <textarea class="form-control" id="notes" name="notes"></textarea>
                                          </div>
                                        </div>

                                        <script>    
                                            function alertmsg() {

                                                swal("Thank you", "We will be in touch shortly");
                                            }

                                        </script>

                                        <!-- Button -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="submit"></label>
                                          <div class="col-md-4">
                                            <button type="submit" class="btn btn-fill" name="submit"  value="Submit" title="Click to Submit" id="form_register_submitbutton"  onclick="PostRecord('ContactRequest', '#commit');alertmsg(); return false;">Submit</button>
                                          </div>
                                        </div>

                                        </fieldset>
                                        </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>      
    </section>
<?php include 'footer_forsale.php' ?>