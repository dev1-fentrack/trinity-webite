<?php
require_once 'db.FT.v1.php';
         
if(!isset($_GET['Uuid'])){
	die(-1);
}

$Uuid=$_GET['Uuid'];


$sqlImage="(select `AssetPictures`.`Image` from AssetPictures where AssetUuid=a.Uuid order by OrderNum desc limit 1 ) as Picture";

$sql="select a.Uuid,".$sqlImage.",Description,Price,FeatureList,Addr1,Addr2,Addr3,Addr4,PostCode,AddrLatLong,BuildType,Bedrooms,Garden,".
	"OffRoadParking,EfficiencyRating,Bathrooms,LivingRooms,Garage,DoubleGlaze,CentralHeat, FloorPlan,Title,".

	"s.Phase From ForSale a left join States s on a.StateId=s.Id ".

	" where a.Uuid='".$Uuid."'";
       

//error_log("sql:" . preg_replace("/\s+/", " ", $sql));



$stmt = $db->prepare($sql);



$stmt->execute();
$result = $stmt->fetchAll();

class Asset {}
$assets = array();

foreach($result as $row) {
	//error_log('del row');
	$e = new Asset();
	$e->Uuid=$row[0];
	$e->Picture=$row[1];
	$e->Description=nl2br ($row[2]);
	$e->Price=$row[3];
	$e->FeatureList=nl2br ($row[4]);
	$e->Addr1=$row[5];
	$e->Addr2=$row[6];
	$e->Addr3=$row[7];
	$e->Addr4=$row[8];
	$e->PostCode=$row[9];
	$e->AddrLatLong=$row[10];
	
	$e->BuildType=$row[11];
	$e->Bedrooms=$row[12];
	$e->Garden=$row[13];
	$e->OffRoadParking=$row[14];
	$e->EfficiencyRating=$row[15];
	$e->Bathrooms=$row[16];
	$e->LivingRooms=$row[17];
	$e->Garage=$row[18];
	$e->DoubleGlaze=$row[19];
	$e->CentralHeat=$row[20];
	$e->FloorPlan=$row[21];
	$e->Title=$row[22];
	$e->Phase=$row[23];

	}

	$assets[] = $e;


header('Content-Type: application/json');
echo json_encode($assets);

?>
