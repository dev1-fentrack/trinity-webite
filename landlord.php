<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Estate Agents for West Yorkshire - Trinity Sales</title>
    <meta name="description" content="Sell in days not months for the market price. Call Trinity Sales Estate Agents Wakefield on 01924 609811 to sell in 28 days.">
	<meta name="keywords" content="Trinity Sales, Local Estate Agents in Wakefield.  Sell your House Fast, Quickly for the Best Price. Flat to buy in Leeds, House to buy in Wakefield, Property to buy Leeds Wakefield, selling in Leeds Wakefield, selling in Leeds, sales in Wakefield, sales in Leeds, Estate Agents ">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'header_forsale.php' ?>
        <div class="container main-section">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="hero-content text-center">
                        <h1>Trinity Estate Agents</h1>
                        <p class="intro">West Yorkshire Estate Agents with a difference</p>
    </section>  
    <section style="padding-bottom: 20px">
        <div class="container">
            <div class="row text-center">
                <h3>Get our free books</h3>
            </div><hr/>
            <div class="row text-center">
                <div class="col-md-6">
                    <h4>Letting a Property For The First Time</h4>
                    <h5><b>10 keys to success when letting your property for the first time</b></h5>
                    <img class="book-img" src="../img/book1.png">
                    <a class="btn btn-fill" href="http://sk123-c717e2.lpp.infusionsoft.com" target="_blank">Get This Free Guide</a>
                </div>
                <div class="col-md-6">
                    <h4>Professional Landlords With Multiple Properties</h4>
                    <h5><b>The seven secrets of highly successful landlords</b></h5>
                    <img class="book-img" src="../img/book2.png">
                    <a class="btn btn-fill" href="http://sk123-19250a.lpp.infusionsoft.com" target="_blank">Get This Free Guide</a>
                </div>
            </div>
            <hr>
        </div>

    </section>

    <section class="selling-video">
        <div class="container">
            <iframe style="display: block; margin: 30px auto;" src="https://player.vimeo.com/video/92036600?color=bdfa05&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </section>
    <section class="features-extra section-padding"  id="scroll_landlord" style="background-color: #F3F4F8">
        <div class="container">
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Landlord Services
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li ><a href="#lettingsprocess" data-toggle="tab">Lettings Process</a>
                                </li>
                                <li><a href="#insurances" data-toggle="tab">Insurances</a>
                                </li>
                                <li class="active" ><a href="#register" data-toggle="tab">Learn More</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div id="appraisal_scroll" class="tab-content">
                                <div class="tab-pane fade in " id="lettingsprocess">
                                    <h3>Lettings, the Trinity way</h3>
                                    <p>How we get your property generating you an income.<br>
                                    How the process will work for you:</p>
                                    <ol>
                                    <li>Take a look at the services we offer on our services page.</li>
                                    <li>Arrange your FREE Property Appraisal where we can discuss your personal situation and advise what is best for you. Either call our 24h landline on 01924 6098111 or Register your details with us and we will contact you to arrange a suitable time to arrange a meeting. The meeting should take 15-30 minutes of your time. We're more than happy to do evening and weekend appointments where our schedule allows.</li>
                                    <li>Once you're ready to proceed, all we need from you is your signature and a set of keys for viewings. We'll do the rest, and we'll get your property immediately marketed via Rightmove, Zoopla etc as well as to our list of registered Tenants on file.</li>
                                    <li>We'll give you regular updates from each viewing that takes place along with any feedback we gain.</li>
                                    <li>Once a tenant applies for your property we'll start to comprehensively check into their references, confirming salary information with their employers/accountants and getting references from previous landlords. This normally takes between 2 and 5 working days.</li>
                                    <li>As soon as a suitable Tenant is found who passes the comprehensive referencing process we will forward their details to you and await your confirmation so we can arrange to get your new Tenant moved in and start paying you your rent as soon as possible. (For those who choose "Tenant Find" our service stops here)</li>
                                    <li>We compile a full inventory and videotaped statement of condition for you, move your new Tenants in and pay the first month's rent minus our charges into your bank account.</li>
                                    Just sit back and enjoy your monthly income from us
                                    <li>Once under management you will get you a monthly statement of account following the payment of rent into your account. This simplified report will come in handy for when you have to submit your self-assessment tax return. All you have to do is occasionally check your emails and wait for us to pay you your monthly income. It couldn't be easier!</li>
                                    </ol>
                                    </div>
                                <div class="tab-pane fade in " id="insurances">
                                    <h3>Legally Require Insurances</h3>
                                    <p>It is a legal requirement that the Landlord maintains the structure of the property. Domestic buildings and contents insurance policies will not cover claims if the property is let to Tenants. It's essential that property owners advise their insurance company they intend to let their property, failure to do so could result in insurances becoming void. Even where a property is unfurnished, items like carpets etc. are not usually covered under buildings insurance. <br>Landlords Contents insurances also include public liability insurance which is vital in order to protect Landlords from any injury claims from either the Tenant or their guests at the property. For example, if a Tenant tripped on loose carpeting, fell down the stairs and broke their leg, leaving then unable to work they could make a claim against the Landlord for damages.</p>
                                    </div>
                                 <div class="tab-pane fade in active" id="register">
                                    <form class="form-modern" id="commit" name="commit">
                                        <input type="hidden" value="callme" name="contactme" id="contactcallme">
                                        <input type="hidden" value="ToLet" name="site" id="site">
                                        <!-- Form Name -->
                                        <h3>Book Your FREE Lettings Appraisal </h3>

                                        <!-- Select Basic -->
                                        <div class="form-input-group">
                                            <i class="fa fa-address-book" aria-hidden="true"></i>
                                            <select id="title" name="title" class="form-control default-option">
                                                <option value="Title">Title</option>
                                                <option value="Mr">Mr</option>
                                                <option value="Mrs">Mrs</option>
                                                <option value="Miss">Miss</option>
                                                <option value="Ms">Ms</option>
                                            </select>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-input-group">
                                            <i class="fa fa-address-book" aria-hidden="true"></i>
                                            <input id="fname" name="fname" type="text" placeholder="Forename" class="form-control input-md" required="">
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-input-group">
                                            <i class="fa fa-address-book" aria-hidden="true"></i> 
                                            <input id="sname" name="sname" type="text" placeholder="Surname" class="form-control input-md" required="">
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-input-group">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <input id="phonew" name="phonew" type="text" placeholder="Phone" pattern="[0][0-9]{10}" class="form-control input-md">
                                        </div>
                                        <div class="form-input-group">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <input id="email" name="email" placeholder="Email" class="form-control input-md" >
                                        </div>
                                        <!-- Reason -->

                                        
                                        <div class="form-input-group">
                                            <i class="fa fa-question" aria-hidden="true"></i>
                                            <select id="enquiry" name="enquiry" class="form-control default-option">
                                                <option value="Enquiry">Enquiry Type</option>
                                                <option value="General">General</option>
                                                <option value="Visit">Visit</option>
                                                <option value="Offer">Offer</option>
                                                <option value="Register">Register</option>
                                            </select>
                                        </div>
                                        

                                        <!-- Multiple Radios (inline) -->
                                        <div class="form-group row text-center">
                                            <div class="col-md-4 checkdiv">
                                                <label class="control-label" for="contact">Contact Time: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <label class="radio-inline no-vertical" for="contact-0">
                                                  <input type="radio" class="option-input radio" name="contact" id="contact-0" value="1" checked="checked">
                                                  8 am - 12 pm
                                                </label> 
                                                <label class="radio-inline no-vertical" for="contact-1">
                                                  <input type="radio" class="option-input radio" name="contact" id="contact-1" value="2">
                                                  12 pm - 5 pm
                                                </label>
                                            </div>
                                        </div>

                                        <!-- Textarea -->
                                        <div class="form-input-group">
                                            <i class="fa fa-sticky-note" aria-hidden="true"></i>                
                                            <textarea class="form-control" id="notes" placeholder="Notes" name="notes"></textarea>
                                        </div>

                                        <!-- Button -->
                                        <div class="form-input-group">
                                            <button type="submit" class="btn-fill btn-submit" name="submit"  value="Submit" title="Click to Submit" id="form_register_submitbutton"  onclick=" PostRecord('ContactRequest', '#commit'); return false;">Submit</button>
                                          </div>
                                        </form>

                                    </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                        </p>
                    </div>
                </div>
            </div>
        </div>      
    </section>
    <section class="blog text-center">
        <div class="container-fluid">
            <div class="row">
                <?php

    $json=file_get_contents("/GetFeaturedData.LM.v1.php?Table=ForSale");

    $data =  json_decode($json);

        $x = 0;
        foreach($data as $object){

        if($x > 0){
            $Pic=$object->{'Picture'};
            if($Pic==''){
                $Pic='img/ap.jpg';
            }else{
                $Pic="http://manyclicks.co.uk/GetImage.v1.php?c1=5426f721e57973808247505426f721e5797380824750&c2=5384eef554b070669042175384eef554b07066904217&filename=".$Pic;
            }

            $Uuid=$object->{'Uuid'};
            $Addr2=$object->{'Addr2'};
            $Addr3=$object->{'Addr3'};

            if($Addr3 != '' | $Addr3 != ' '){
                $Addr2.=", ";
            }
            $BuildType=$object->{'BuildType'};
            
     

            echo '  <div class="col-md-4">
                    <article class="blog-post">
                        <figure>
                            <a href="'.$Pic.'" class="single_image">
                                <div class="blog-img-wrap">
                                    <div class="overlay">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    <img src="'.$Pic.'" alt=""/>
                                </div>
                            </a>
                            <figcaption>
                            <h3><a  class="blog-category" >'.$Addr2.$Addr3.'</a></h3>
                            <p><a href="'.$ForSaleUrl.'/propertyview.php?Uuid='.$Uuid.'" class="blog-post-title">'.$BuildType.'<i class="fa fa-angle-right"></i></a></p>
                            </figcaption>
                        </figure>
                    </article>
                </div>';

                
        }
        $x++;
    }


?>

            </div>
        </div>
    </section>
<?php include 'testimonials.php' ?>
<?php include 'footer_forsale.php' ?>