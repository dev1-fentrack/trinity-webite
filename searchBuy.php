<!-- Search Box w/ Buy as Primary -->

<div class="searchbox-area">
            <div id="searchbox" class="col-lg-12">
                    <ul class="tabsearch">
                        <li><a href="#" class="tablinks tabactive">Buy</a></li>
                        <li><a href="<?php echo $AltUrl; ?>"  class="tablinks tabready">Rent</a></li>
                    </ul>

                    <div id="Buy" class="tabsearch-content text-center" style="display: block;">
                                         <form class="form-inline">
                                         <div class="form-group">
                                            <label class="sr-only" for="SaleLocation">Location</label>
                                            <select class="form-control" id="SaleLocation">
                                                <option value="">Any Location</option>
                                                <option id="Ackworth" value="Ackworth">Ackworth</option>
                                                <option id="Barnsley" value="Barnsley">Barnsley</option>
                                                <option id="Bradford" value="Bradford">Bradford</option>
                                                <option id="Castleford" value="Castleford">Castleford</option>
                                                <option id="Knottingley" value="Knottingley">Knottingley</option>
                                                <option id="Leeds" value="Leeds">Leeds</option>
                                                <option id="Ossett" value="Ossett">Ossett</option>
                                                <option id="Wakefield" value="Wakefield">Wakefield</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label class="sr-only" for="SaleMinPrice">Minimum Price</label>
                                            <select class="form-control" id="SaleMinPrice">
                                            <option selected="selected" value="0">No Minimum</option>
                                            <option value="25000">&pound; 25,000</option><option value="50000">&pound; 50,000</option><option value="75000">&pound; 75,000</option><option value="100000">&pound; 100,000</option><option value="125000">&pound; 125,000</option><option value="150000">&pound; 150,000</option><option value="175000">&pound; 175,000</option><option value="200000">&pound; 200,000</option><option value="225000">&pound; 225,000</option><option value="250000">&pound; 250,000</option><option value="275000">&pound; 275,000</option><option value="300000">&pound; 300,000</option><option value="325000">&pound; 325,000</option><option value="350000">&pound; 350,000</option><option value="375000">&pound; 375,000</option><option value="400000">&pound; 400,000</option><option value="425000">&pound; 425,000</option><option value="450000">&pound; 450,000</option><option value="475000">&pound; 475,000</option><option value="500000">&pound; 500,000</option><option value="550000">&pound; 550,000</option><option value="600000">&pound; 600,000</option><option value="650000">&pound; 650,000</option><option value="700000">&pound; 700,000</option><option value="750000">&pound; 750,000</option><option value="800000">&pound; 800,000</option><option value="850000">&pound; 850,000</option><option value="900000">&pound; 900,000</option><option value="950000">&pound; 950,000</option><option value="1000000">&pound; 1,000,000</option><option value="1100000">&pound; 1,100,000</option><option value="1200000">&pound; 1,200,000</option><option value="1300000">&pound; 1,300,000</option><option value="1400000">&pound; 1,400,000</option><option value="1500000">&pound; 1,500,000</option><option value="1600000">&pound; 1,600,000</option><option value="1700000">&pound; 1,700,000</option><option value="1800000">&pound; 1,800,000</option><option value="1900000">&pound; 1,900,000</option><option value="2000000">&pound; 2,000,000</option><option value="3000000">&pound; 3,000,000</option><option value="4000000">&pound; 4,000,000</option><option value="5000000">&pound; 5,000,000</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label class="sr-only" for="SaleMaxPrice">Maximum Price</label>
                                            <select class="form-control" id="SaleMaxPrice">
                                            <option selected="selected" value="0">No Maximum</option>
                                            <option value="25000">&pound; 25,000</option><option value="50000">&pound; 50,000</option><option value="75000">&pound; 75,000</option><option value="100000">&pound; 100,000</option><option value="125000">&pound; 125,000</option><option value="150000">&pound; 150,000</option><option value="175000">&pound; 175,000</option><option value="200000">&pound; 200,000</option><option value="225000">&pound; 225,000</option><option value="250000">&pound; 250,000</option><option value="275000">&pound; 275,000</option><option value="300000">&pound; 300,000</option><option value="325000">&pound; 325,000</option><option value="350000">&pound; 350,000</option><option value="375000">&pound; 375,000</option><option value="400000">&pound; 400,000</option><option value="425000">&pound; 425,000</option><option value="450000">&pound; 450,000</option><option value="475000">&pound; 475,000</option><option value="500000">&pound; 500,000</option><option value="550000">&pound; 550,000</option><option value="600000">&pound; 600,000</option><option value="650000">&pound; 650,000</option><option value="700000">&pound; 700,000</option><option value="750000">&pound; 750,000</option><option value="800000">&pound; 800,000</option><option value="850000">&pound; 850,000</option><option value="900000">&pound; 900,000</option><option value="950000">&pound; 950,000</option><option value="1000000">&pound; 1,000,000</option><option value="1100000">&pound; 1,100,000</option><option value="1200000">&pound; 1,200,000</option><option value="1300000">&pound; 1,300,000</option><option value="1400000">&pound; 1,400,000</option><option value="1500000">&pound; 1,500,000</option><option value="1600000">&pound; 1,600,000</option><option value="1700000">&pound; 1,700,000</option><option value="1800000">&pound; 1,800,000</option><option value="1900000">&pound; 1,900,000</option><option value="2000000">&pound; 2,000,000</option><option value="3000000">&pound; 3,000,000</option><option value="4000000">&pound; 4,000,000</option><option value="5000000">&pound; 5,000,000</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label class="sr-only" for="SaleNumBeds">Number of Beds</label>
                                            <select class="form-control" id="SaleNumBeds">
                                              <option>Any Beds</option>
                                              <option>Studio</option>
                                              <option>1</option>
                                              <option>2</option>
                                              <option>3</option>
                                              <option>4</option>
                                              <option>5+</option>
                                            </select>
                                          </div>
                                            <div class="form-group">
                                            <label class="sr-only" for="SalePropType">Type of Property</label>
                                            <select class="form-control" id="SalePropType">
                                                <option value="">Any Type</option>
                                                <option value="House">House</option>
                                                <option value="Flat">Flat</option>
                                                <option value="" disabled="">&nbsp;</option>
                                                <option value="Apartment">Apartment</option>
                                                <option value="Bungalow">Bungalow</option>
                                                <option value="Cottage">Cottage</option>
                                                <option value="Detached">Detached</option>
                                                <option value="End Of Terrace">End Of Terrace</option>
                                                <option value="Semi Detached">Semi Detached</option>
                                                <option value="Terraced">Terraced</option>
                                                <option value="Town House">Town House</option>
                                            </select>
                                          </div>
                                          <button onclick="SearchForSale();return false;" class="btn-search btn-fill btn-margin-right">Submit</button>
                                        </form>

                                </div>

                    <div id="Rent" class="tabsearch-content">
                                        <form class="form-inline">
                                         <div class="form-group" align="left">
                                            <label class="sr-only" for="letLocation">Location</label>
                                            <select class="form-control" id="LetLocation">
                                                <option value="">Any Location</option>
                                                <option id="Ackworth" value="Ackworth">Ackworth</option>
                                                <option id="Barnsley" value="Barnsley">Barnsley</option>
                                                <option id="Bradford" value="Bradford">Bradford</option>
                                                <option id="Castleford" value="Castleford">Castleford</option>
                                                <option id="Knottingley" value="Knottingley">Knottingley</option>
                                                <option id="Leeds" value="Leeds">Leeds</option>
                                                <option id="Ossett" value="Ossett">Ossett</option>
                                                <option id="Wakefield" value="Wakefield">Wakefield</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label class="sr-only" for="LetMinPrice">Minimum Price</label>
                                            <select class="form-control" id="LetMinPrice">
                                              <option value="">No Minimum</option><option value="300">&pound; 300 pcm</option><option value="350">&pound; 350 pcm</option><option value="400">&pound; 400 pcm</option><option value="450">&pound; 450 pcm</option><option value="500">&pound; 500 pcm</option><option value="550">&pound; 550 pcm</option><option value="600">&pound; 600 pcm</option><option value="650">&pound; 650 pcm</option><option value="700">&pound; 700 pcm</option><option value="750">&pound; 750 pcm</option><option value="800">&pound; 800 pcm</option><option value="850">&pound; 850 pcm</option><option value="900">&pound; 900 pcm</option><option value="950">&pound; 950 pcm</option><option value="1000">&pound; 1000 pcm</option><option value="1100">&pound; 1100 pcm</option><option value="1200">&pound; 1200 pcm</option><option value="1300">&pound; 1300 pcm</option><option value="1400">&pound; 1400 pcm</option><option value="1500">&pound; 1500 pcm</option><option value="1600">&pound; 1600 pcm</option><option value="1700">&pound; 1700 pcm</option><option value="1800">&pound; 1800 pcm</option><option value="1900">&pound; 1900 pcm</option><option value="2000">&pound; 2000 pcm</option><option value="2500">&pound; 2500 pcm</option><option value="3000">&pound; 3000 pcm</option><option value="3500">&pound; 3500 pcm</option><option value="4000">&pound; 4000 pcm</option><option value="4500">&pound; 4500 pcm</option><option value="5000">&pound; 5000 pcm</option><option value="6000">&pound; 6000 pcm</option><option value="7000">&pound; 7000 pcm</option><option value="8000">&pound; 8000 pcm</option><option value="9000">&pound; 9000 pcm</option><option value="10000">&pound; 10000 pcm</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label class="sr-only" for="LetMaxPrice">Maximum Price</label>
                                            <select class="form-control" id="LetMaxPrice">
                                            <option value="">No Maximum</option><option value="300">&pound; 300 pcm</option><option value="350">&pound; 350 pcm</option><option value="400">&pound; 400 pcm</option><option value="450">&pound; 450 pcm</option><option value="500">&pound; 500 pcm</option><option value="550">&pound; 550 pcm</option><option value="600">&pound; 600 pcm</option><option value="650">&pound; 650 pcm</option><option value="700">&pound; 700 pcm</option><option value="750">&pound; 750 pcm</option><option value="800">&pound; 800 pcm</option><option value="850">&pound; 850 pcm</option><option value="900">&pound; 900 pcm</option><option value="950">&pound; 950 pcm</option><option value="1000">&pound; 1000 pcm</option><option value="1100">&pound; 1100 pcm</option><option value="1200">&pound; 1200 pcm</option><option value="1300">&pound; 1300 pcm</option><option value="1400">&pound; 1400 pcm</option><option value="1500">&pound; 1500 pcm</option><option value="1600">&pound; 1600 pcm</option><option value="1700">&pound; 1700 pcm</option><option value="1800">&pound; 1800 pcm</option><option value="1900">&pound; 1900 pcm</option><option value="2000">&pound; 2000 pcm</option><option value="2500">&pound; 2500 pcm</option><option value="3000">&pound; 3000 pcm</option><option value="3500">&pound; 3500 pcm</option><option value="4000">&pound; 4000 pcm</option><option value="4500">&pound; 4500 pcm</option><option value="5000">&pound; 5000 pcm</option><option value="6000">&pound; 6000 pcm</option><option value="7000">&pound; 7000 pcm</option><option value="8000">&pound; 8000 pcm</option><option value="9000">&pound; 9000 pcm</option><option value="10000">&pound; 10000 pcm</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <label class="sr-only" for="LetNumBeds">Number of Beds</label>
                                            <select class="form-control" id="LetNumBeds">
                                              <option>Any Beds</option>
                                              <option>Studio</option>
                                              <option>1</option>
                                              <option>2</option>
                                              <option>3</option>
                                              <option>4</option>
                                              <option>5+</option>
                                            </select>
                                          </div>
                                            <div class="form-group">
                                            <label class="sr-only" for="LetPropType">Type of Property</label>
                                            <select class="form-control" id="LetPropType">
                                                <option value="">Any Type</option>
                                                <option value="House">House</option>
                                                <option value="Flat">Flat</option>
                                                <option value="" disabled="">&nbsp;</option>
                                                <option value="Apartment">Apartment</option>
                                                <option value="Bungalow">Bungalow</option>
                                                <option value="Cottage">Cottage</option>
                                                <option value="Detached">Detached</option>
                                                <option value="End Of Terrace">End Of Terrace</option>
                                                <option value="Semi Detached">Semi Detached</option>
                                                <option value="Terraced">Terraced</option>
                                                <option value="Town House">Town House</option>
                                            </select>
                                          </div>
                                          <button href="" target="_blank" onclick="SearchForSale();return false;" class="btn-search btn-fill btn-margin-right">Submit</button>
                                        </form>
                                </div>
                     </div>
                </div>
            </div>
        </div>
        </div><br>