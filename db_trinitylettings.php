<?php
require_once("config_trinitylettings.php");

function FixDoubleQuote($db_val){
    //        return htmlentities($db_val);
    return str_replace('"', '\"', $db_val);

}

function FixSingleQuote($db_val){
    
    $ret=htmlspecialchars($db_val);

    
	return $ret;
//        return str_replace('"', '\"', $db_val);

//        return str_replace("'", "\'", $db_val);
}


function FixSingleQuoteWithHtmlDecode($db_val){
    
    $ret=htmlspecialchars($db_val);

    //this is a temp test
    $ret=htmlspecialchars_decode($ret);

    return $ret;
//        return str_replace('"', '\"', $db_val);

//        return str_replace("'", "\'", $db_val);
}


class Db {
    public static function connect() {
        $con = mysql_connect(Config::server(), Config::username(), Config::password());
        if (!$con) {
            die('Could not connect: ' . mysql_error());
        }
        return $con;
    }

    public static function open($dbname, $con) {
        $ret = "";
        if (Config::db() == "") {
            $ret = mysql_select_db($dbname, $con);
        } else {
            $ret = mysql_select_db(Config::db(), $con);
        }
        return $ret;
    }
}

?>
