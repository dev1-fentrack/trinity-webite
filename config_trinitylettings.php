<?php

//define ("HEROKU", "1");

class Config {

    private static function getUrl() {
        return parse_url(getenv("CLEARDB_DATABASE_URL"));
    }

    //not perfect but the first step to removing all the ip refernces in the php. 
    //at least it's a single place to swtich between http://localhost & https://ext ip fro now

    public static function protocol_prefix_and_server() {
        return 'https://127.0.0.1';
        //return 'http://localhost';

    }



    public static function server() {
        $server = "127.0.0.1";
        if (defined('HEROKU')) {
            $url = Config::getUrl();
            $server = $url["host"];
        }
        //error_log("server:".$server);

        return $server;
    }

    public static function username() {
        $username = "root";
        if (defined('HEROKU')) {
            $url = Config::getUrl();
            $username = $url["user"];
        }
        return $username;
    }

    public static function password() {
        $password = "happy@lm";
        if (defined('HEROKU')) {
            $url = Config::getUrl();
            $password = $url["pass"];
        }
        return $password;
    }

    public static function db() {
        $db = "";
        if (defined('HEROKU')) {
            $url = Config::getUrl();
            $db = substr($url["path"],1);
        }
        return $db;
    }
}

?>
